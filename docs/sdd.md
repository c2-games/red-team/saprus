# Saprus Software Design Document (SDD)

## Table of Contents <!-- omit from toc -->

- [1. Introduction](#1-introduction)
  - [1.1 Document Purpose](#11-document-purpose)
  - [1.2 Saprus Scope](#12-saprus-scope)
  - [1.3 Definitions, Acronyms, and Abbreviations](#13-definitions-acronyms-and-abbreviations)
  - [1.4 References](#14-references)
- [2. System Overview](#2-system-overview)
  - [2.1 Major System Components](#21-major-system-components)
  - [2.2 Key Features and Functionality (MVP)](#22-key-features-and-functionality-mvp)
  - [2.3 Future Releases](#23-future-releases)
  - [2.4 Example Deployed Architecture](#24-example-deployed-architecture)
- [3. Design Considerations](#3-design-considerations)
  - [3.1 Assumptions and Dependencies](#31-assumptions-and-dependencies)
  - [3.2 Constraints](#32-constraints)
  - [3.3 Risk Analysis](#33-risk-analysis)
- [4. Design](#4-design)
  - [4.1 Saprus Protocol Design](#41-saprus-protocol-design)
  - [4.2 Saprus Sentinel Design](#42-saprus-sentinel-design)
  - [4.3 Saprus Command (SapCom) Design](#43-saprus-command-sapcom-design)
  - [4.4 Saprus Pass-Watch Design](#44-saprus-pass-watch-design)
- [5. Testing Strategy](#5-testing-strategy)
  - [5.1 Unit Testing](#51-unit-testing)
  - [5.2 Integration Testing](#52-integration-testing)
  - [5.3 System Testing](#53-system-testing)

## 1. Introduction

### 1.1 Document Purpose

The purpose of this Software Design Document (SDD) is to provide a comprehensive description of the architecture and design for the Saprus software system.
This document serves as a guide for the development team during the implementation phase, ensuring that all aspects of the software design are thoroughly planned and documented.

The SDD outlines the key features and functionality that will be included in the Minimum Viable Product (MVP) release, as well as those planned for future iterations.
It provides detailed information about the system’s architecture, data models, user interface, and integration points.
Additionally, the SDD identifies design considerations, constraints, assumptions, and potential risks associated with the project.

This document is intended for use by the development team, project managers, quality assurance teams, and other stakeholders involved in the project.
It serves as a reference throughout the development lifecycle, facilitating communication, ensuring alignment with the project’s objectives, and guiding the decision-making process.

### 1.2 Saprus Scope

Saprus is intended for use by cybersecurity Red Teams for white box cybersecurity testing, hackathon capture the flag (CTF) competitions, or other similar purposes by cybersecurity teams.

Saprus is comprised of several major components; the Saprus protocol, Saprus Command (aka SapCom), Saprus Sentinel, and the Saprus Clients.
I high-level overview of each is below:

- **Saprus** - A protocol which allows for the transmission of data and files with built-in obscurity, layered on top of the UDP protocol.
  All components of the Saprus system leverage the Saprus protocol.
- **SapCom** - A command and control application which controls Saprus Clients through Saprus Sentinels.
- **Saprus Sentinel** - A data collection and relay server which can relay and distribute data between Saprus Clients and SapCom either unidirectionally or bidirectionally.
- **Saprus Clients** - A suite of micro-applications, each with a single purpose that are deployed onto target systems.
  Saprus Clients are intended to be transparent, obfuscated, and hidden from the end users of the systems.

### 1.3 Definitions, Acronyms, and Abbreviations

| Term | Definition |
| --- | --- |
| CTF | Capture the Flag |
| IRC | <!-- TODO: Define IRC --> XXXX |
| Red Team | A cybersecurity team that simulates real-world attacks on an organization's systems to identify vulnerabilities. |
| SapCom | Saprus Command |
| SDD | Software Design Document |
| TLS | Transport Layer Security |
| White Box | A method of security testing where the tester has full knowledge of the system's internal structure, source code, architecture, and configurations. |

### 1.4 References

- MinIO - <https://github.com/minio/minio>

## 2. System Overview

### 2.1 Major System Components

This section outlines the major system components, their primary purpose, and other related details explaining the components.

#### Saprus Protocol

The Saprus protocol is built on top of UDP with the intent of passing data, command, files, and other information between Saprus components in an obscure fashion.
The protocol is intended to be lightweight, operate on systems without established IP addresses where necessary, circumvent firewalls, and be hidden from system owners.

The protocol is fully defined in the [Saprus Protocol Design](./saprus_protocl_design.md) document.

#### Saprus Command (SapCom)

Saprus Command, or SapCom, is a command and control application for a deployed Saprus implementation.
SapCom's primary purpose is to send instructions to Saprus Clients via Saprus Sentinels and gather and collate data from Saprus Sentinels.

SapCom operates within the Red Team's control and functions as a combined IRC, file, noSQL database, HTTP, and Saprus relay server, each configured as a microservice.
These microservices serve the following purposes:

- **Rocket Chat** - Saprus Sentinels will join the Rocket Chat server to receive instructions from and send messages to the SapCom server.
- **File Server** - Stores, receives, and sends files to and from Saprus Sentinels.
  Uses block storage as a backend for storing and sending files ([MinIO](https://github.com/minio/minio)).
- **NoSQL Database** - Stores data received from Saprus Sentinels in a for which can be easily sorted, collated, queried, etc.
- **HTTP/REST API Server** - Serves as an interface for the various other SapCom functions.
- **Saprus Relay** - Sends and receives communications over the Saprus protocol in cases where communications between SapCom and Sentinels need to be obfuscated.
  Leverages the Saprus Protocol as a wrapper around other protocols (IRC, HTTP, etc.) to achieve obfuscation.

#### Saprus Sentinel

Saprus Sentinel is a data collection and relay server which can relay and distribute data between Saprus Clients and SapCom either unidirectionally or bidirectionally.
Sentinels sit on a network within the broadcast domain of Saprus Client host systems.

Sentinel receives data from any Clients within its network and relays the data from the clients to SapCom.
Sentinel can also receive data from SapCom with instructions or other data to be distributed to Clients.

Sentinel leverages the Saprus Protocol for communications with Clients.
Communications from clients are generally received using raw socket communications within broadcast packets, with the intent of bypassing most firewalls.

Sentinel typically communicates with SapCom over traditional TCP/IP/UDP protocols, but may leverage the Saprus Relay function of SapCom to obfuscate traffic using the Saprus Protocol in cases where such obfuscation is desirable.
Sentinel operators decide at the time of Sentinel deployment how they will communicate to SapCom.

#### Saprus Clients

Saprus Clients are a suite of micro-applications, each with a single purpose that are deployed onto target systems.
Saprus Clients are intended to be transparent, obfuscated, and hidden from the end users of the systems.
Each Client performs one or more very specific functions, each one intended to operate independently.

Saprus Clients communicate with Saprus Sentinels over the Saprus Protocol to send and receive data.
As each Client is autonomous, exactly how and which data they communicate will depend on the function of the client.

The following Clients have thus far been defined, though additional clients will be be added in future:

##### Log-Watch

Watches for changes to configured log files on the system and sends the changed line to Sentinel for relay back to SapCom.

##### Spawner

Spawns or deploys additional Saprus Clients on the host system based on either pre-packaged instructions or instructions received from SapCom via a Sentinel.

### 2.2 Key Features and Functionality (MVP)

<!-- List and describe the key features and functionality that will be included in the MVP release. This section should focus on the core features that are critical to the system’s operation. -->

- SapCom
  - Rocket Chat instance
  - Ability to send/receive data to/from Sentinels
  - Ability to send commands to Sentinels
- Sentinels
  - Setup to talk to SapCom via Rocket Chat
  - Proper bi-directional communications to clients - reliable and standardized
- Clients
  - 3 basic functions
    - Message relaying
    - Command execution
    - Spawner initial POC - make file uploads work (stretch goal)

### 2.3 Future Releases

Outline the features and functionality planned for future releases. Prioritize these features and provide an estimated timeline for their development.

### 2.4 Example Deployed Architecture

```mermaid
flowchart TD
  subgraph Saprus Network
    sapcom[(SapCom)]
  end

  subgraph Network 1
    subgraph Saprus 1
      sen1[(Sentinel 1)] <--> sapcom
    end
    subgraph Host 1
      lw1[Log-Watch] --> sen1
      spawn1[Spawner] <--> sen1
    end
    subgraph Host 2
      lw2[Log-Watch] --> sen1
      spawn2[Spawner] <--> sen1
    end
  end

  subgraph Network 2
    subgraph Saprus 2
      sen2[(Sentinel 2)] <--> sapcom
    end
    subgraph Host 3
      lw3[Log-Watch] --> sen2
      spawn3[Spawner] <--> sen2
    end
    subgraph Host 4
      lw4[Log-Watch] --> sen2
      spawn4[Spawner] <--> sen2
    end
  end
```

## 3. Design Considerations

### 3.1 Assumptions and Dependencies

#### 3.1.1 Assumptions

- Saprus components will be deployed as part of a controlled environment, where the system owners are aware of and and are leveraging Saprus.
- Systems which host saprus components are either built with Saprus as part of their configuration (IaC) or the system owners assist in deploying Saprus components to those systems.

#### 3.1.2 Dependencies

- Saprus clients and Sentinels must reside within the same broadcast domain.

### 3.2 Constraints

Saprus is developed as an open source utility without dedicated support.
As such, it bears all the same constraints of any other open source tool including:

- Minimal funding
- Development performed as developers have time and interest in the project.

### 3.3 Risk Analysis

Saprus is developed as an open source utility without dedicated support.
As such, it bears all the same potential risks of any other open source tool, including:

- Minimal funding
- Potential for misuse by external parties

## 4. Design

Each component of Saprus has its own design considerations.
This section will outline the design of the major components.

### 4.1 Saprus Protocol Design

The Saprus Protocol design is outlined in the following document: [Saprus Protocol Design](./saprus_protocl_design.md)

### 4.2 Saprus Sentinel Design

Saprus Sentinel is written in Golang and heavily leverages Goroutines for simultaneous processing of data from multiple Client requests and an optionally configured SapCom instance.

Sentinel operates by listening to UPD4 packets from the network on which it resides, as well as listening for REST API calls from a SapCom instance it is paired to.

Sentinel pairs to a SapCom instance via a pre-shared key (Rocket Chat bot token) and configured hostname or IP address.

#### 4.2.1 Saprus Client Communications to Sentinel

Saprus Sentinel listens on all IP addresses assigned to it for UDP4 packets.
Sentinel listens both to specific ports, as well as any broadcast packets.
If Sentinel detects a Saprus Protocol Packet, it will automatically read in and spawn a ew Go routine to process that packet.

Depending on the content of the received packet, Sentinel will do one or more of the following:

- Gather data about the Client that sent the packet to be able to initiate future communications with the Client.
  - Sentinel will make a determination on whether the Client it receives the packet from is a Client with which it should be communicating based on one or more of the following:
    - Saprus Protocol type
    - Pre-shared secrets
    - Encryption key
    - Hidden data within the packet header
    - Implementation-specific formatting of the packet
    - Other implementation-specific criteria
- Send data back to the client based on a request in the packet.
- Display received messages from the Client.
- If Saprus Sentinel is configured to talk to a SapCom instance, it will then pass any received data back to SapCom.

#### 4.2.2 SapCom Communications to Sentinel

Sentinel can receive data from SapCom over its exposed APIs.
These APIs will process commands from SapCom to perform any number of actions related to the the Sentinel or Saprus Clients including:

- Sending a request to a client to return specific data.
- Sending a request to a client to perform a given action the client is capable of.
- Returning data the Sentinel already possesses.
- Change configuration of the Sentinel or Clients.

#### 4.2.3 Sentinel Communications to Clients

Sentinel will send communications to any Clients it is aware of via Saprus Protocol packets.
Based on previous communications with the Client, Sentinel will determine how best to communicate with the Client.
All communications are expected to take place using the Saprus Protocol, but as the protocol allows for a wide range of implementations, it is impossible to clearly define all the potential scenarios.

Sentinel will typically send the following types of requests to clients:

- Requests for specific data.
- Client shutdown or restart commands.
- Requests for the client to perform a given action the client is capable of.

#### 4.2.4 Sentinel Communications to SapCom

Sentinel will communicate to a configured SapCom instance over regular TCP/IP protocols using REST APIs, FTP, Rocket Chat, or other configured methods the SapCom instance supports.
Sentinel will typically send the following types of communications to SapCom:

- Relay data from Clients.
- Status information.
- Errors or other detected problems.
- Information on the Clients the Sentinel has communicated with and/or is aware of.

<!-- TODO: Finish this section -->

### 4.3 Saprus Command (SapCom) Design

SapCom is a micro-service based collection of services which provides storage, management, administration, and interfacing capabilities to the Saprus system.
The core of SapCom is based around a customized Rocket Chat instance which leverages Rocket Chat bots to send and receive information from Saprus Sentinels and Clients.
SapCom also leverages a NoSQL database for storing and working with information received the rest of the Saprus system.

<!-- TODO: Finish this section -->

### 4.4 Saprus Pass-Watch Design

<!-- TODO: Finish this section -->

<!--
### 4.1 Component Design

### 4.2 Data Model

Describe the data structures and database schema that the system will use. Include an ER diagram if applicable.

### 4.3 API Design

Detail the design of any APIs that the system will expose. Include endpoints, request/response formats, and authentication mechanisms.

### 4.4 User Interface Design

Describe the user interface design, including wire-frames or mockups. Explain how the UI will evolve from the MVP to subsequent releases.
-->

## 5. Testing Strategy

### 5.1 Unit Testing

Describe the unit testing strategy, including tools, frameworks, and coverage goals.

### 5.2 Integration Testing

Explain the approach to integration testing, focusing on how different components of the system will be tested together.

### 5.3 System Testing

Detail the system testing strategy, including performance, security, and usability testing.
