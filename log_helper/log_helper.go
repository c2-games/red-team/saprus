package log_helper

import (
	"fmt"
	"log"
	"os"
)

// LogLevel represents different debug levels
type LogLevel int

const (
	// LevelNone disables all debug output
	LevelNone LogLevel = iota
	// LevelError shows only error messages
	LevelError
	// LevelWarn shows warning and error messages
	LevelWarn
	// LevelInfo shows info, warning and error messages
	LevelInfo
	// LevelDebug shows all debug messages
	LevelDebug
)

// Logger holds the application's debug level configuration
type Logger struct {
	appLevel LogLevel
	logger   *log.Logger
}

// getLevelPrefix returns the appropriate prefix for the log level
func (d *Logger) getLevelPrefix(level LogLevel) string {
	switch level {
	case LevelError:
		return "ERROR: "
	case LevelWarn:
		return "WARN: "
	case LevelInfo:
		return "INFO: "
	case LevelDebug:
		return "DEBUG: "
	default:
		return ""
	}
}

// SetLevel changes the debug level at runtime
func (d *Logger) SetLevel(level LogLevel) {
	d.appLevel = level
}

// GetLevel returns the current debug level
func (d *Logger) GetLevel() LogLevel {
	return d.appLevel
}

// Message prints the given message if the message's level is less than or equal to the application's debug level
func (d *Logger) Message(level LogLevel, message string) {
	if level <= d.appLevel {
		prefix := d.getLevelPrefix(level)
		d.logger.Printf("%s%s\n", prefix, message)
	}
}

// Messagef prints the given formatted message if the message's level is less than or equal to the application's debug level
func (d *Logger) Messagef(level LogLevel, format string, args ...interface{}) {
	if level <= d.appLevel {
		prefix := d.getLevelPrefix(level)
		d.logger.Printf("%s"+format, append([]interface{}{prefix}, args...)...)
	}
}

// Convenience methods for different log levels
func (d *Logger) Error(message string) {
	d.Message(LevelError, message)
}

func (d *Logger) Errorf(format string, args ...interface{}) {
	d.Messagef(LevelError, format, args...)
}

func (d *Logger) Warn(message string) {
	d.Message(LevelWarn, message)
}

func (d *Logger) Warnf(format string, args ...interface{}) {
	d.Messagef(LevelWarn, format, args...)
}

func (d *Logger) Info(message string) {
	d.Message(LevelInfo, message)
}

func (d *Logger) Infof(format string, args ...interface{}) {
	d.Messagef(LevelInfo, format, args...)
}

func (d *Logger) Debug(message string) {
	d.Message(LevelDebug, message)
}

func (d *Logger) Debugf(format string, args ...interface{}) {
	d.Messagef(LevelDebug, format, args...)
}

// NewLogger creates a new Logger instance with the specified application debug level
func NewLogger(appLevel LogLevel) *Logger {
	return &Logger{
		appLevel: appLevel,
		logger:   log.New(os.Stderr, "", log.LstdFlags),
	}
}

// NewLoggerWithLogger creates a new Logger instance with a custom logger
func NewLoggerWithLogger(appLevel LogLevel, logger *log.Logger) *Logger {
	return &Logger{
		appLevel: appLevel,
		logger:   logger,
	}
}

// NewLoggerFromInt creates a new Logger instance from an integer value (0-4).
// 0: None, 1: Error, 2: Warn, 3: Info, 4: Debug.
// Returns an error if the level is invalid.
func NewLoggerFromInt(level int) (*Logger, error) {
	if level < 0 || level > 4 {
		return nil, fmt.Errorf("invalid debug level: %d (must be 0-4)", level)
	}
	return NewLogger(LogLevel(level)), nil
}

// NewLoggerFromIntMust creates a new Logger instance from an integer value (0-4).
// Panics if the level is invalid.
func NewLoggerFromIntMust(level int) *Logger {
	d, err := NewLoggerFromInt(level)
	if err != nil {
		panic(err)
	}
	return d
}
