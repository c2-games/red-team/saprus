# Sentinel

## Building

### Using Make

```bash
# Build with cache
make build-sentinel

# Build without cache
make build-sentinel-clean
```

### Development Build

```bash
make sentinel-build-dev
```

## Running

### Using Docker Compose

The Sentinel is configured to run in host network mode to allow direct UDP access:

```bash
# From sapcom directory
make compose-up
```

### Environment Variables

- `SENTINEL_NAME` - Name of this sentinel instance
- `SENTINEL_PSK` - Pre-shared key for registration
- `SENTINEL_PORT` - UDP port to listen on (default: 8888)
- `SENTINEL_SAPCOM_REST_URL` - URL of SapCom API
- `SENTINEL_SAPCOM_REST_PORT` - Port of SapCom API
- `SENTINEL_SAPCOM_NATS_URL` - NATS server URL
- `SENTINEL_SAPCOM_NATS_PORT` - NATS server port
- `SENTINEL_SAPCOM_NATS_USER` - NATS username
- `SENTINEL_SAPCOM_NATS_PASS` - NATS password

### Dependencies

Sentinel requires:

- Healthy SapCom API service
- NATS connectivity
