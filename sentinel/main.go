package main

import (
	"saprus/logo"
	"saprus/sentinel/cmd"
)

func main() {
	logo.WelcomeMessage()

	cmd.Execute()
}
