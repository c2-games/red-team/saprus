// This file contains functions, etc. related to NATS

package core

import (
	"fmt"
	"log"
	"strings"
	"time"

	nh "saprus/nats_helper"

	"github.com/nats-io/nats.go"
)

func sapComNatsConnect(s *Settings) {
	log.Println("Starting NATS connection.")
	var natsURL = fmt.Sprintf("%s:%d", s.SapComNatsURL, s.SapComNatsPort)

	onSuccess := func() {
		err := natsRealConnect(natsURL, s)
		if err != nil {
			log.Printf("failed NATS connection: %v", err)
		}
	}

	if len(s.Token) > 0 {
		log.Printf("DEBUG: Using existing token")
		if len(s.NatsSubject) == 0 {
			sanitizedName := strings.ReplaceAll(s.Name, "-", "_")
			s.NatsSubject = fmt.Sprintf("sentinel.%s", sanitizedName)
			if err := writeToYAMLFile(runningConfig, runningConfig.Config); err != nil {
				log.Fatalf("failed updating config file: %v", err)
			}
		}
		onSuccess()
	} else {
		log.Printf("DEBUG: No existing token, requesting new token")

		if err := natsInitialConnect(natsURL, s); err != nil {
			log.Printf("failed NATS initial connection: %v", err)
		}

		log.Printf("DEBUG: NATS initial connection successful")
		onSuccess()
	}
}

func natsInitialConnect(url string, s *Settings) error {
	var msgJSON nh.TokenReqResp
	var err error

	log.Printf("DEBUG: Requesting token from NATS")
	if err = nh.RequestTokenAnon(url, s.PSK, s.Name, &msgJSON); err != nil {
		return fmt.Errorf("failed getting token: %v", err)
	}

	s.Token = msgJSON.Token
	s.NatsSubject = msgJSON.Subject
	if err = writeToYAMLFile(runningConfig, runningConfig.Config); err != nil {
		return fmt.Errorf("failed updating config file: %w", err)
	}

	return nil
}

func natsRealConnect(url string, s *Settings) error {
	var err error

	log.Printf("DEBUG: Attempting real NATS connection")
	// Attempt to connect to NATS every 3 seconds until successful
	natsConn, err = nh.NatsConnectRetrier(url, s.SapComNatsUser, s.SapComNatsPass, 3*time.Second, 100)
	if err != nil {
		return fmt.Errorf("failed to establish real NATS connection: %v", err)
	}

	// Subscribe to NATS instructions subject
	_, err = nh.NewNatsSub(natsConn, s.NatsSubject+".instructions", natsInstructionsHandler)
	if err != nil {
		return fmt.Errorf("failed subscribing to NATS instructions subject: %v", err)
	}
	log.Println("DEBUG: Subscribed to NATS instructions subject")
	// Subscribe to NATS all sentinels instructions subject
	_, err = nh.NewNatsSub(natsConn, "sentinel.all.instructions", natsInstructionsHandler)
	if err != nil {
		return fmt.Errorf("failed to subscribing to NATS all sentinels instructions subject: %v", err)
	}
	log.Println("DEBUG: Subscribed to NATS all sentinels instructions subject")

	// DEBUG: Test sending a message to the NATS relayMessages subject
	log.Println("DEBUG: Sending test message to NATS relayMessages subject")
	if err := forwardRelayMessageNats("DEBUG: Test message from sentinel: " + s.Name); err != nil {
		log.Printf("failed sending message to NATS relayMessages subject: %v", err)
	}

	sapComAuth = 1
	log.Println("DEBUG: NATS connection successful")
	close(natsConnReady) // Signal connection is ready
	return nil
}

func forwardRelayMessageNats(m string) error {
	if natsConn == nil || !natsConn.IsConnected() {
		return fmt.Errorf("NATS connection not established")
	}

	jsonBody, err := constructRelayMessage(m)
	if err != nil {
		return fmt.Errorf("failed constructing relay message JSON for NATS: %v", err)
	}

	subject := runningConfig.NatsSubject + ".relayMessages"
	log.Printf("DEBUG: Publishing to NATS subject: %s", subject)
	log.Printf("DEBUG: Message: %s", m)
	log.Printf("DEBUG: JSON Body: %s", string(jsonBody))

	var msg = nats.Msg{
		Subject: subject,
		Data:    jsonBody,
	}

	if err := natsConn.PublishMsg(&msg); err != nil {
		log.Printf("ERROR: Failed to publish message: %v", err)
		return fmt.Errorf("failed publishing message: %v", err)
	}

	log.Printf("DEBUG: Successfully published message to NATS")
	return nil
}

func natsInstructionsHandler(msg *nats.Msg) {
	// TODO: Write this function once we have instructions to send to sentinels
}
