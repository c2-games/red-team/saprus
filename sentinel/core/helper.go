package core

import (
	"regexp"
	"strings"
)

// sanitizeString cleans up a string by:
// 1. Trimming leading and trailing whitespace.
// 2. Replacing spaces and special characters with underscores.
func sanitizeString(input string) string {
	// Trim leading and trailing whitespace
	s := strings.TrimSpace(input)

	// Replace spaces and special characters with underscores
	re := regexp.MustCompile(`[^\w]+`) // Matches any non-word character
	s = re.ReplaceAllString(s, "_")

	// Remove any leading or trailing underscores
	s = strings.Trim(s, "_")

	return s
}
