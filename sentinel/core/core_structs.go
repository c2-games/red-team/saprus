// This file contains the global structs for the core package.

package core

type Settings struct {
	// File path to the startup config file
	Config string `yaml:"config"`
	// The name of the Sentinel
	Name string `yaml:"name"`
	// The NATS subject for the Sentinel
	NatsSubject string `yaml:"nats-subject"`
	// Port to listen for messages on
	Port int `yaml:"port"`
	// Pre-shared key
	PSK string `yaml:"psk"`
	// URL of the SapCom REST API server
	SapComRestURL string `yaml:"sapcom-rest-url"`
	// Port of the SapCom REST server
	SapComRestPort int `yaml:"sapcom-rest-port"`
	// URL of the SapCom NATS server
	SapComNatsURL string `yaml:"sapcom-nats-url"`
	// User to connect to NATS with
	SapComNatsUser string `yaml:"sapcom-nats-user"`
	// Password to connect to NATS with
	SapComNatsPass string `yaml:"sapcom-nats-pass"`
	// Port of the SapCom NATS server
	SapComNatsPort int `yaml:"sapcom-nats-port"`
	// Token to authenticate to SapCom with (if pre-authenticated)
	Token string `yaml:"token"`
}

type SentinelRegisterRequest struct {
	Psk  string `json:"psk"`
	Name string `json:"name"`
}

// Struct for the registerSentinel response
type SentinelRegisterResponse struct {
	// Unique token to authorize Sentinel to send & receive messages
	Token string `json:"token"`
	// Main NATS subject for the Sentinel to listen on and send messages to
	Subject string `json:"subject"`
}

type relayMessageJson struct {
	// The sentinel that received the relay message
	Sentinel string `json:"sentinel"`
	// The message that was received
	Message string `json:"message"`
	// Timestamp in format: 2006-01-02 15:04:05 MST
	Timestamp string `json:"timestamp"`
}
