// This file contains the code relating to authenticating to a SapCom REST API server

package core

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

func sapComRestConnect(s Settings) {
	var url string = s.SapComRestURL + ":" + fmt.Sprint(s.SapComRestPort)

	// Define the onSuccess function that should be executed when "pong" is received
	onSuccess := func() {
		if len(runningConfig.Token) > 0 {
			// Check to make sure we can connect with the token
			if err := authPing(url, s.Name); err != nil {
				log.Printf("ERROR: Unable to validate connection to Sentinel token: %s. Giving up.", err)
			}
		} else {
			if err := registerSentinelREST(); err != nil {
				log.Printf("ERROR: Unable to register Sentinel using Pre-Shared Key: %s. Giving up.", err)
			} else {
				if err := authPing(url, s.Name); err != nil {
					log.Printf("ERROR: Unable to validate connection to Sentinel token: %s. Giving up.", err)
				}
			}
		}
	}

	// Create a done channel to notify when the ping loop is done
	pingDone := make(chan bool)

	// Start the pinging in a goroutine
	go pingServer(url+"/ping", pingDone, onSuccess)
}

// Function to continuously ping the server every 10 seconds until a "pong" is received
func pingServer(url string, done chan bool, onSuccess func()) {
	for {
		// Ping the server
		resp, err := http.Get(url)
		if err != nil {
			log.Printf("Error pinging SapCom server: %v\n", err)
		} else {
			defer resp.Body.Close()

			// Read the response body
			body, err := io.ReadAll(resp.Body)
			if err != nil {
				log.Printf("Error reading SapCom server response: %v\n", err)
				continue
			}

			// Check if the response is "pong"
			if string(body) == "pong" {
				// log.Println("Received pong! Executing onSuccess function.")
				onSuccess()
				done <- true // Notify that the task is done
				return       // Exit the goroutine
			}
		}

		// Wait for 10 seconds before the next ping
		time.Sleep(10 * time.Second)
	}
}

// Function to connect (authPing) to the SapCom server to validate connection and token authentication.
func authPing(url, sentinelName string) error {
	// Struct for the response body
	type PingResponse struct {
		Message string `json:"message"`
	}

	var pingUrl string = url + "/authPing"

	// Create a new request
	req, err := http.NewRequest("GET", pingUrl, nil)
	if err != nil {
		return fmt.Errorf("error creating request: %w", err)
	}

	// Set the headers
	req.Header.Set("Authorization", runningConfig.Token)
	req.Header.Set("Sentinel-Name", sentinelName)

	// Send the request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error making GET request: %w", err)
	}
	defer resp.Body.Close()

	// Check the response status
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("received non-200 response: %d", resp.StatusCode)
	}

	// Read the response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("error reading response body: %w", err)
	}

	// Parse the JSON response
	var pingResp PingResponse
	if err := json.Unmarshal(body, &pingResp); err != nil {
		return fmt.Errorf("error parsing response: %w", err)
	}

	// Check if the response message is "pong"
	if pingResp.Message == "pong" {
		sapComAuth = 2 // Set the global variable
		log.Println("Authenticated to SapCom.")
	} else {
		return fmt.Errorf("unexpected authPing response message: %s", pingResp.Message)
	}

	return nil
}

// Function to connect to the SapCom server with the pre-shared key and get a token.
func registerSentinelREST() error {
	// Create the request payload
	payload := SentinelRegisterRequest{
		Psk:  runningConfig.PSK,
		Name: runningConfig.Name,
	}

	// URL for the REST API - used if NATS connection not available
	var authUrl string = runningConfig.SapComRestURL + ":" +
		fmt.Sprint(runningConfig.SapComRestPort) + "/registerSentinel"

	// Convert the payload to JSON
	jsonData, err := json.Marshal(payload)
	if err != nil {
		return fmt.Errorf("error marshalling JSON: %w", err)
	}

	// Send the POST request
	resp, err := http.Post(authUrl, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return fmt.Errorf("error making POST request: %w", err)
	}
	defer resp.Body.Close()

	// Check the response status
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("received non-200 response: %d", resp.StatusCode)
	}

	// Read the response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("error reading response body: %w", err)
	}

	// Parse the JSON response
	var registerResp SentinelRegisterResponse
	if err := json.Unmarshal(body, &registerResp); err != nil {
		return fmt.Errorf("error parsing response: %w", err)
	}

	// Update the global token variable with the new token
	runningConfig.Token = registerResp.Token
	if err = writeToYAMLFile(runningConfig, runningConfig.Config); err != nil {
		return fmt.Errorf("failed updating config file: %w", err)
	}

	return nil
}
