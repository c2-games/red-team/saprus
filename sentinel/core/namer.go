// This file is for generating a random name for the Sentinel instance, if none is provided.

package core

import (
	"math/rand"
)

func genRandomName() string {

	adjectives := []string{
		"Bubbly", "Whimsical", "Zany", "Frolicsome", "Wobbly", "Dazzling", "Snazzy", "Quirky", "Sparkling", "Glittery",
		"Jolly", "Sprightly", "Gleeful", "Bouncy", "Wacky", "Snappy", "Peculiar", "Giggly", "Cheery", "Playful",
		"Twinkling", "Witty", "Goofy", "Luminous", "Silly", "Dreamy", "Blissful", "Chipper", "Vivacious", "Quixotic",
		"Fanciful", "Whirly", "Giddy", "Perky", "Jazzy", "Zesty", "Glimmering", "Snuggly", "Jubilant", "Spunky",
		"Spritely", "Merry", "Dandy", "Zippy", "Radiant", "Ticklish", "Loopy", "Buoyant", "Festive", "Effervescent",
	}

	creatures := []string{
		"Dragon", "Griffin", "Phoenix", "Minotaur", "Chimera", "Kraken", "Basilisk", "Manticore", "Hydra", "Cyclops",
		"Cerberus", "Gorgon", "Unicorn", "Sphinx", "Leviathan", "Yeti", "Banshee", "Pegasus", "Centaur", "Wendigo",
		"Vampire", "Werewolf", "Troll", "Goblin", "Ogre", "Djinn", "Fairy", "Nymph", "Selkie", "Sasquatch",
		"Mermaid", "Harpy", "Lich", "Wraith", "Zombie", "Ghoul", "Dullahan", "Kappa", "Kelpie", "Naga",
		"Balrog", "Ent", "Direwolf", "Hobgoblin", "Rakshasa", "Doppelganger", "Beholder", "Owlbear", "MindFlayer", "Demogorgon",
	}

	randomAdjective := adjectives[rand.Intn(len(adjectives))]
	randomCreature := creatures[rand.Intn(len(creatures))]

	return randomAdjective + randomCreature
}
