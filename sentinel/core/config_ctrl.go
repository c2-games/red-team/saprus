// This file contains the code relating to reading/writing the config file.

package core

import (
	"fmt"
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

// configFileSetup checks if a config file path has been set, the validates that it's usable for storing config data. If not, it picks /opt/saprus/config/config.yaml as the path.
func configFileSetup(path string) error {
	var pathsToCheck []string
	var err error

	if len(path) > 3 {
		pathsToCheck = append(pathsToCheck, path)
	}

	pathsToCheck = append(pathsToCheck, "/opt/sentinel/config/config.yaml")

	for i := range pathsToCheck {
		var goodPath bool
		goodPath, err = checkFileAccessible(pathsToCheck[i])
		if goodPath {
			runningConfig.Config = pathsToCheck[i]
			if err := writeToYAMLFile(runningConfig, runningConfig.Config); err != nil {
				return fmt.Errorf("failed writing config file: %w", err)
			}
			log.Printf("Using config file for settings storage at: %s", runningConfig.Config)
			return nil
		}
	}

	return fmt.Errorf("no writable path for config file: %w", err)
}

// checkFileAccessible checks if the file at the given path exists and is both readable and writable.
// If the file does not exist, it attempts to create it.
func checkFileAccessible(path string) (bool, error) {
	// Check if the file exists
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		// Try to create the file if it does not exist
		file, err := os.Create(path)
		if err != nil {
			return false, fmt.Errorf("file does not exist and could not be created: %s, error: %w", path, err)
		}
		file.Close() // Close immediately after creating to release the handle
		// fmt.Printf("File created at path: %s\n", path)
		return true, nil
	} else if err != nil {
		return false, fmt.Errorf("error accessing file: %w", err)
	}

	// Check if it's a file (not a directory)
	if info.IsDir() {
		return false, fmt.Errorf("path is a directory, not a file: %s", path)
	}

	// Check if the file is both readable and writable by trying to open it in read-write mode
	file, err := os.OpenFile(path, os.O_RDWR, 0644)
	if err != nil {
		return false, fmt.Errorf("file is not accessible for reading and writing: %s", path)
	}
	defer file.Close()

	return true, nil
}

// Function to write struct to a YAML file
func writeToYAMLFile(settings Settings, filePath string) error {
	// Marshal the struct into YAML format
	data, err := yaml.Marshal(&settings)
	if err != nil {
		return fmt.Errorf("failed to marshal struct to YAML: %w", err)
	}

	// Write the YAML data to the specified file
	err = os.WriteFile(filePath, data, 0644)
	if err != nil {
		return fmt.Errorf("failed to write YAML to file: %w", err)
	}

	return nil
}
