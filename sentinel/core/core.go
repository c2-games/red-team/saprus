// The main package for the core functionality of Sentinel.
package core

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"saprus/structs"
	"saprus/utils"
)

func Startup(s Settings, ctx context.Context) error {
	log.Println("Starting Sentinel")
	runningConfig = s

	// Set a name for the Sentinel if there isn't one
	if runningConfig.Name == "" {
		runningConfig.Name = genRandomName()
		log.Printf("Using randomly generated name: %s", runningConfig.Name)
	}
	if err := configFileSetup(runningConfig.Config); err != nil {
		return fmt.Errorf("no writeable config file: %w", err)
	}

	// Sanitize the name
	runningConfig.Name = sanitizeString(runningConfig.Name)

	switch {
	// If we have connection info for SapCom NATS, connect to it
	case len(runningConfig.SapComNatsURL) >= 4 && len(fmt.Sprint(runningConfig.SapComNatsPort)) >= 2 &&
		len(runningConfig.SapComNatsUser) >= 3 && len(runningConfig.SapComNatsPass) >= 8:
		go sapComNatsConnect(&runningConfig)
	// If we don't have have connection info for NATS, but do for SapCom REST API, connect to it
	case len(runningConfig.SapComNatsURL) < 4 && len(runningConfig.SapComRestURL) >= 4 &&
		len(fmt.Sprint(runningConfig.SapComRestPort)) >= 2 && (len(runningConfig.PSK) < 6 || len(runningConfig.Token) < 6):
		go sapComRestConnect(runningConfig)
	}

	// Start the server process to listen for Saprus packets from Clients
	serv, err := net.ListenPacket("udp4", "0.0.0.0:"+fmt.Sprint(runningConfig.Port))
	if err != nil {
		return fmt.Errorf("error in server startup: %w", err)
	}
	defer serv.Close()

	// Saprus packet buffer
	buff := make([]byte, 2048)

	for {
		select {
		// Listen for the context cancellation signal
		case <-ctx.Done():
			log.Println("Shutdown signal received, shutting down server...")
			return nil // Exit the loop and return from the function

		default:
			// Listen for incoming packets
			serv.SetReadDeadline(time.Now().Add(1 * time.Second)) // Set a timeout to allow checking the context
			n, addr, err := serv.ReadFrom(buff)
			if err != nil {
				// If the error is a timeout, continue the loop and check ctx.Done
				if ne, ok := err.(net.Error); ok && ne.Timeout() {
					continue
				}
				log.Printf("Error reading from buffer: %v\n", err)
				continue
			}

			// Handle the request in a separate goroutine
			go ProcessRequests(buff[:n], addr)
		}
	}
}

func ProcessRequests(buf []byte, addr net.Addr) {
	c := utils.SocketCall{}

	saprusHeader := c.GatherSaprusHeader(buf)
	remoteAddr := strings.Split(addr.String(), ":")

	c.Iface = utils.FindOriginInterface(remoteAddr)
	if (c.Iface == nil) && (remoteAddr[0] != "0.0.0.0") {
		log.Printf("ERROR: Iface was nil -- remoteAddr: %s\n", remoteAddr[0])
	}

	switch {
	// File transfer type
	case saprusHeader.PacketType == [2]byte{0x88, 0x88}:
		log.Printf("INFO: Incomplete function requested by:: %s\n", remoteAddr)
		c.SendData(base64.StdEncoding.EncodeToString([]byte("hello\n")))
	// Relay message type
	case saprusHeader.PacketType == [2]byte{0x00, 0x3C}:
		handleRelayMessage(c, buf)
	// Unknown Saprus type
	default:
		// FIXME: Should return error?:
		// return fmt.Errorf("saprus type not found:%x", string(saprusHeader.PacketType[:]))
		log.Printf("DEBUG: Saprus type not found::%x\n", string(saprusHeader.PacketType[:]))
	}

	clear(buf)
	return
}

func handleRelayMessage(c utils.SocketCall, buf []byte) {
	var relayMessage = parseRelayMessageToString(c, buf)

	switch sapComAuth {
	case 1:
		select {
		case <-natsConnReady:
			if err := forwardRelayMessageNats(relayMessage); err != nil {
				log.Printf("ERROR: failed relaying message to SapCom via NATS: %s", err)
			}
		case <-time.After(5 * time.Second):
			log.Printf("ERROR: Timed out waiting for NATS connection")
		}
	case 2:
		if err := forwardRelayMessageRest(relayMessage); err != nil {
			log.Printf("ERROR: failed relaying message to SapCom via REST API: %s", err)
		}
	}

	logRelayMessage(relayMessage)
}

// Gets string data from a provided Saprus packet (buf)
func parseRelayMessageToString(c utils.SocketCall, buf []byte) string {
	saprusPacket := c.GatherSaprusHeader(buf)
	relayMessage := structs.ParseRelayMessage(saprusPacket.Payload)
	// Strip null bytes from the end of the payload
	relayMessage.Payload = bytes.TrimRight(relayMessage.Payload, "\x00")
	return string(relayMessage.Payload)
}

// Forward the received relay message to the SapCom server
func forwardRelayMessageRest(m string) error {
	var targetURL string = runningConfig.SapComRestURL + ":" +
		fmt.Sprint(runningConfig.SapComRestPort) + "/relayMessage"

	jsonBody, err := constructRelayMessage(m)
	if err != nil {
		return fmt.Errorf("failed constructing relay message JSON for REST: %v", err)
	}

	// Create an io.Reader from the JSON byte slice
	requestBody := bytes.NewReader(jsonBody)

	// Create a new request
	req, err := http.NewRequest("POST", targetURL, requestBody)
	if err != nil {
		return fmt.Errorf("error creating request: %w", err)
	}

	// Set the headers
	req.Header.Set("Authorization", runningConfig.Token)
	req.Header.Set("Sentinel-Name", runningConfig.Name)

	req.Header.Set("Content-Type", "application/json")

	// Send the request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error sending relay message to SapCom: %w", err)
	}
	defer resp.Body.Close()

	// Check the response status
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("received non-200 response: %d", resp.StatusCode)
	}

	return nil
}

// Construct the relay message as a JSON
func constructRelayMessage(m string) ([]byte, error) {
	now := time.Now()
	timestamp := now.Format("2006-01-02 15:04:05 MST")

	var body = relayMessageJson{
		Sentinel:  runningConfig.Name,
		Message:   m,
		Timestamp: timestamp,
	}

	// Convert the struct to JSON
	jsonBody, err := json.Marshal(body)
	if err != nil {
		return []byte{}, fmt.Errorf("failed to marshal the body JSON: %w", err)
	}

	return jsonBody, err
}

// Log the relay message
func logRelayMessage(m string) {
	log.Printf("INFO: Relay message received: %s", m)
}
