// This file contains all the global variables for the 'core' package.

package core

import "github.com/nats-io/nats.go"

var runningConfig Settings

var natsConn *nats.Conn

// Var that tracks how SapCom is connected.
// 0 = No SapCom connection.
// 1 = NATS connection.
// 2 = REST API connection.
var sapComAuth int = 0

var natsConnReady = make(chan struct{})
