#!/bin/bash

set -e

for team in {13..24}; do

cat <<EOF>"saprus-team${team}.yml"
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
  labels:
    service: saprus
    team: team${team}
    perspective: internal
  name: team${team}-internal-saprus
  namespace: red-team
spec:
  replicas: 0
  selector:
    matchLabels:
      service: saprus
      team: team${team}
      perspective: internal
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      annotations:
        k8s.v1.cni.cncf.io/networks: default/attachnet
        ovn.kubernetes.io/logical_switch: ovn-default
        attachnet.default.ovn.kubernetes.io/logical_switch: team${team}
      labels:
        service: saprus
        team: team${team}
        perspective: internal
    spec:
      containers:
        - image: registry.gitlab.com/c2-games/red-team/saprus/saprus-sentinel:4
          imagePullPolicy: Always
          name: sentinel
          ports:
            - containerPort: 8888
              protocol: UDP
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          securityContext:
            capabilities:
              add: ["NET_ADMIN", "NET_RAW"]
          env:
            - name: SENTINEL_NAME
              value: "sentinel-team${team}"
            - name: SENTINEL_PSK
              value: "soul shiny logo ribs pupil tight"
            - name: SENTINEL_PORT
              value: "8888"
            - name: SENTINEL_SAPCOM_REST_URL
              value: "http://172.18.1.30"
            - name: SENTINEL_SAPCOM_REST_PORT
              value: "14227"
            - name: SENTINEL_SAPCOM_NATS_URL
              value: "nats://172.18.1.30"
            - name: SENTINEL_SAPCOM_NATS_PORT
              value: "4222"
            - name: SENTINEL_SAPCOM_NATS_USER
              value: "senuser"
            - name: SENTINEL_SAPCOM_NATS_PASS
              value: "senpass1"
          volumeMounts:
            - name: config-volume-${team}
              mountPath: /opt/sentinel/config/
      volumes:
        - name: config-volume-${team}
          emptyDir: {}
      dnsConfig:
        searches:
          - ncaecybergames.org
        options:
          - name: ndots
            value: "1"
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      terminationGracePeriodSeconds: 30
...
EOF

done
