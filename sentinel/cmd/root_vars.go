package cmd

import "saprus/sentinel/core"

var startSettings = core.Settings{
	Port:           8888,
	SapComRestPort: 14227,
	SapComNatsPort: 4222,
}
