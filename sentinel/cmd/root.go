package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"saprus/sentinel/core"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "sentinel",
	Short: "TODO",
	Long:  `TODO`,
	Run: func(cmd *cobra.Command, args []string) {
		// Set up signal handling context
		ctx := setupSignalHandler()

		if err := startup(ctx); err != nil {
			fmt.Printf("Application error: %v\n", err)
			os.Exit(1)
		}

		fmt.Println("Application exited gracefully.")
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(&startSettings.SapComNatsURL, "sapcom-nats-url", "b", startSettings.SapComNatsURL,
		"SapCom NATS server URL")
	viper.BindPFlag("sapcom-nats-url", rootCmd.PersistentFlags().Lookup("sapcom-nats-url"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.Config, "config", "c", "",
		"Config file path (default is /etc/sentinel/config.yaml)")
	viper.BindPFlag("config", rootCmd.PersistentFlags().Lookup("config"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.PSK, "psk", "k", "",
		"Pre-shared authentication key")
	viper.BindPFlag("psk", rootCmd.PersistentFlags().Lookup("psk"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.Name, "name", "n", startSettings.Name,
		"Sets the unique name of this Saprus Sentinel for communicating with SapCom")
	viper.BindPFlag("name", rootCmd.PersistentFlags().Lookup("name"))

	rootCmd.PersistentFlags().IntVarP(&startSettings.SapComRestPort, "sapcom-rest-port", "o", startSettings.SapComRestPort,
		"SapCom REST server port")
	viper.BindPFlag("sapcom-rest-port", rootCmd.PersistentFlags().Lookup("sapcom-rest-port"))

	rootCmd.PersistentFlags().IntVarP(&startSettings.Port, "port", "p", startSettings.Port,
		"Port for Sentinel to listen on")
	viper.BindPFlag("port", rootCmd.PersistentFlags().Lookup("port"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.SapComNatsPass, "sapcom-nats-pass", "P", startSettings.SapComNatsPass,
		"SapCom NATS server password")
	viper.BindPFlag("sapcom-nats-pass", rootCmd.PersistentFlags().Lookup("sapcom-nats-pass"))

	rootCmd.PersistentFlags().IntVarP(&startSettings.SapComNatsPort, "sapcom-nats-port", "s", startSettings.SapComNatsPort,
		"SapCom NATS server port")
	viper.BindPFlag("sapcom-nats-port", rootCmd.PersistentFlags().Lookup("sapcom-nats-port"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.Token, "token", "t", "",
		"Token for authentication to SapCom")
	viper.BindPFlag("token", rootCmd.PersistentFlags().Lookup("token"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.SapComRestURL, "sapcom-rest-url", "u", startSettings.SapComRestURL,
		"SapCom REST server URL")
	viper.BindPFlag("sapcom-rest-url", rootCmd.PersistentFlags().Lookup("sapcom-rest-url"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.SapComNatsUser, "sapcom-nats-user", "U", startSettings.SapComNatsUser,
		"SapCom NATS server username")
	viper.BindPFlag("sapcom-nats-user", rootCmd.PersistentFlags().Lookup("sapcom-nats-user"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if startSettings.Config != "" {
		// Use config file from the flag.
		viper.SetConfigFile(startSettings.Config)
	} else {
		// Search config in default directories directory with name "config" (without extension).
		viper.AddConfigPath("/etc/sentinel")
		viper.AddConfigPath("/opt/sentinel/config")
		viper.AddConfigPath("./config")
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
		startSettings.Config = viper.ConfigFileUsed()
	}

	// Setup ENV vars in format of "SAPCOM_VAR". Ex: SAPCOM_DB_URL; SAPCOM_API_PORT
	// ENV vars will take precedence over settings defined in the config file
	viper.SetEnvPrefix("SENTINEL")
	// Replace hyphens with underscores in keys
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	// Read in environment variables that match
	viper.AutomaticEnv()

	// Set the vars from switches; takes precedence over ENV vars and Config file.
	cmd := rootCmd
	cmd.Flags().Visit(func(f *pflag.Flag) {
		// Only override if flag was explicitly set
		if f.Changed {
			viper.Set(f.Name, f.Value.String())
		}
	})

	// Set the vars from switches; takes precedence over ENV vars and Config file.
	setVars()
	// preStart()
}

func setVars() {
	startSettings.Config = viper.GetString("config")
	startSettings.Name = viper.GetString("name")
	startSettings.Port = viper.GetInt("port")
	startSettings.PSK = viper.GetString("psk")
	startSettings.SapComRestURL = viper.GetString("sapcom-rest-url")
	startSettings.SapComRestPort = viper.GetInt("sapcom-rest-port")
	startSettings.SapComNatsURL = viper.GetString("sapcom-nats-url")
	startSettings.SapComNatsPort = viper.GetInt("sapcom-nats-port")
	startSettings.SapComNatsUser = viper.GetString("sapcom-nats-user")
	startSettings.SapComNatsPass = viper.GetString("sapcom-nats-pass")
	startSettings.Token = viper.GetString("token")

	startSettings.Name = viper.GetString("name")

	if len(startSettings.Name) < 3 {
		log.Println("WARNING: No name for this Sentinel provided. " +
			"A random name will be generated.")
	}

	if len(startSettings.SapComNatsURL) < 4 || len(fmt.Sprint(startSettings.SapComNatsPort)) < 2 ||
		len(startSettings.SapComNatsUser) < 3 || len(startSettings.SapComNatsPass) < 8 {
		log.Println("WARNING: Invalid NATS configuration; will not be able to connect via NATS")

		if len(startSettings.SapComRestURL) < 4 || len(fmt.Sprint(startSettings.SapComRestPort)) < 2 ||
			(len(startSettings.PSK) < 6 && len(startSettings.Token) < 6) {
			log.Println("WARNING: Invalid REST API configuration;" +
				" will not be able to connect to SapCom via REST API")
		}
	}
}

func setupSignalHandler() context.Context {
	ctx, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	go func() {
		sig := <-c
		fmt.Printf("Received signal %s. Shutting down...\n", sig)
		cancel()
	}()

	return ctx
}

func startup(ctx context.Context) error {
	err := core.Startup(startSettings, ctx)
	if err != nil {
		return fmt.Errorf("startup failed: %w", err)
	}

	return nil
}
