module saprus/sentinel

go 1.23.4

replace saprus/utils => ../utils

replace saprus/structs => ../structs

replace saprus/logo => ../logo

replace saprus/nats_helper => ../nats_helper

require (
	github.com/nats-io/nats.go v1.38.0
	github.com/spf13/cobra v1.8.1
	github.com/spf13/viper v1.19.0
	gopkg.in/yaml.v3 v3.0.1
	saprus/logo v0.0.0-00010101000000-000000000000
	saprus/nats_helper v0.0.0-00010101000000-000000000000
	saprus/structs v0.0.0-00010101000000-000000000000
	saprus/utils v0.0.0-00010101000000-000000000000
)

require (
	github.com/fsnotify/fsnotify v1.8.0 // indirect
	github.com/google/gopacket v1.1.19 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/josharian/native v1.1.0 // indirect
	github.com/klauspost/compress v1.17.11 // indirect
	github.com/magiconair/properties v1.8.9 // indirect
	github.com/mdlayher/packet v1.1.2 // indirect
	github.com/mdlayher/socket v0.5.1 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/nats-io/nkeys v0.4.9 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/pelletier/go-toml/v2 v2.2.3 // indirect
	github.com/sagikazarmark/locafero v0.7.0 // indirect
	github.com/sagikazarmark/slog-shim v0.1.0 // indirect
	github.com/sourcegraph/conc v0.3.0 // indirect
	github.com/spf13/afero v1.12.0 // indirect
	github.com/spf13/cast v1.7.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/subosito/gotenv v1.6.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.32.0 // indirect
	golang.org/x/exp v0.0.0-20250106191152-7588d65b2ba8 // indirect
	golang.org/x/net v0.34.0 // indirect
	golang.org/x/sync v0.10.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
	golang.org/x/text v0.21.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
