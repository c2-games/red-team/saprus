# Saprus

Saprus is an open source tool that leverages a custom protocol (Saprus Protocol) to monitor systems, inject data and files, and perform other actions against target systems.
Saprus was designed by and built to support the cyber competitions and cybersecurity hackathons.

## System Design

See the [Saprus Software Design Document](./docs/sdd.md) for detailed information on the design of Saprus.

## Building Saprus

See individual component READMEs for build instructions:

- [SapCom](./sapcom/README.md)
- [Sentinel](./sentinel/README.md)

## Deploying Saprus

### Prerequisites

- Docker Engine 20.10.0+
- Docker Compose v2.0.0+
- Git

### Quick Start

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/c2-games/red-team/saprus.git
    cd saprus
    ```

2. Initialize environment configuration:

    ```bash
    make init-env
    ```

3. Review and modify configuration in `sapcom/.env` as needed

    ```bash
    cd sapcom
    # Edit the .env file
    vim sapcom/.env
    ```

4. Start the stack:

    ```bash
    make compose-up
    ```

### Available Make Commands

#### Environment Commands

- `make init-env` - Create initial environment configuration
  - Creates `sapcom/.env` file

#### Build Commands

- `make build-all` - Build all images using cache
- `make build-all-clean` - Build all images without cache
- `make build-api` - Build API image
- `make build-api-clean` - Build API image without cache
- `make build-overlord` - Build Overlord image
- `make build-overlord-clean` - Build Overlord image without cache
- `make build-sentinel` - Build Sentinel image
- `make build-sentinel-clean` - Build Sentinel image without cache
- `make build-sapcom-all` - Build all SapCom images locally

#### Compose Commands

- `make compose-up` - Start all services
- `make compose-down` - Stop all services
- `make compose-down-volumes` - Stop services and remove volumes
- `make compose-logs` - View service logs
- `make compose-ps` - Check service status
- `make compose-validate` - Validate compose configuration
- `make compose-build` - Build service images
- `make compose-pull` - Pull latest service images
- `make compose-restart` - Restart all services

#### Development Commands

- `make dev-init` - Initialize development environment
- `make dev-clean` - Clean up development environment
- `make dev-build` - Build all images locally
- `make dev-up` - Start all services in development mode
- `make dev-down` - Stop all services in development mode

For more detailed deployment instructions and configuration options, see [SapCom README](./sapcom/README.md).

## Contributing to Saprus

<!-- TODO: Write this section -->

## License

See the [Saprus License here](./LICENSE).
