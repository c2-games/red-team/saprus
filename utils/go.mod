module saprus/utils

go 1.22.2

require (
	github.com/google/gopacket v1.1.19
	github.com/mdlayher/packet v1.1.2
	saprus/structs v0.0.0-00010101000000-000000000000
)

require (
	github.com/josharian/native v1.1.0 // indirect
	github.com/mdlayher/socket v0.4.1 // indirect
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
)

replace saprus/structs => ../structs
