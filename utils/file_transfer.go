package utils

import (
	"log"

	"github.com/google/gopacket"

	"saprus/structs"
)

func (c *SocketCall) BuildFileTransferPacket(Header structs.SaprusHeaderFrame,
	Data structs.SaprusFileTransfer) gopacket.SerializeBuffer {
	eth, ip, udp := basePacket(c)

	sapHeader := structs.SaprusHeaderFrame{
		PacketType: Header.PacketType,
		Length:     c.saprusPayloadLength(Data.Bytes()),
		Payload:    Data.Bytes(),
	}

	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{
		ComputeChecksums: true,
		FixLengths:       true,
	}

	udp.SetNetworkLayerForChecksum(&ip)
	err := gopacket.SerializeLayers(buf, opts, &eth, &ip, &udp, gopacket.Payload(sapHeader.Bytes()))

	if err != nil {
		log.Panic(err)
	}

	return buf
}

func (c *SocketCall) FileTransferPacket(message string, seqNum uint16) gopacket.SerializeBuffer {
	sapFile := structs.SaprusFileTransfer{
		Flag:     [1]byte{0x11},
		Sequence: [2]byte{byte(seqNum >> 8), byte(seqNum & 0xFF)},
		Checksum: [2]byte{0x44, 0x44},
		Payload:  []byte(message),
	}

	sapHeader := structs.SaprusHeaderFrame{
		PacketType: [2]byte{0x88, 0x88},
		Length:     c.saprusPayloadLength(sapFile.Bytes()),
		Payload:    sapFile.Bytes(),
	}

	buf := c.BuildFileTransferPacket(sapHeader, sapFile)

	return buf
}

func (c *SocketCall) InitFileTransferPacket() gopacket.SerializeBuffer {
	sapFile := structs.SaprusFileTransfer{
		Flag:     [1]byte{0x01},
		Sequence: [2]byte{0x00, 0x00},
		Checksum: [2]byte{0x00, 0x00},
		Payload:  []byte{},
	}

	sapHeader := structs.SaprusHeaderFrame{
		PacketType: [2]byte{0x00, 0x00},
		Length:     c.saprusPayloadLength(sapFile.Bytes()),
		Payload:    sapFile.Bytes(),
	}

	buf := c.BuildFileTransferPacket(sapHeader, sapFile)

	return buf
}

func (c *SocketCall) EndFileTransferPacket() gopacket.SerializeBuffer {
	sapFile := structs.SaprusFileTransfer{
		Flag:     [1]byte{0x02},
		Sequence: [2]byte{0x00, 0x00},
		Checksum: [2]byte{0x00, 0x00},
		Payload:  []byte{},
	}

	sapHeader := structs.SaprusHeaderFrame{
		PacketType: [2]byte{0x00, 0x00},
		Length:     c.saprusPayloadLength(sapFile.Bytes()),
		Payload:    sapFile.Bytes(),
	}

	buf := c.BuildFileTransferPacket(sapHeader, sapFile)

	return buf
}
