package utils

import (
	"encoding/binary"
	"errors"
	"time"

	"net"
	"unicode/utf8"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/mdlayher/packet"

	"saprus/structs"
)

type SocketCall struct {
	conn  *packet.Conn
	Iface *net.Interface
}

func splitData(message string) []string {
	data := []string{}
	maxPacketLength := 1449

	var i, j int

	for i, j = 0, maxPacketLength; j < len(message); i, j = j, j+maxPacketLength {
		for !utf8.RuneStart(message[j]) {
			j--
		}
		data = append(data, message[i:j])
	}
	data = append(data, message[i:])

	return data
}

func (c *SocketCall) GatherSaprusHeader(buf []byte) structs.SaprusHeaderFrame {
	data := structs.SaprusHeaderFrame{
		PacketType: [2]byte{buf[0], buf[1]},
		Length:     [2]byte{buf[2], buf[3]},
		Payload:    make([]byte, len(buf[4:])),
	}
	copy(data.Payload, buf[4:])
	return data
}

func (c *SocketCall) SendPackage(buf gopacket.SerializeBuffer) error {
	_, err := c.conn.WriteTo(buf.Bytes(), &packet.Addr{HardwareAddr: layers.EthernetBroadcast})
	return err
}

// Send a message string
func (c *SocketCall) SendData(message string) error {
	if c.Iface == nil {
		//lint:ignore ST1005 - that's the name of the struct object
		return errors.New("Iface was null")
	}

	conn, err := packet.Listen(c.Iface, packet.Raw, int(layers.EthernetTypeIPv4), nil)
	if err != nil {
		// FIXME: You shouldn't have a panic nested in a module like this.
		// Just return an error.
		// Orig: log.Panic(err)
		return errors.New("failed to open packet sockets:" + err.Error())
	}
	c.conn = conn

	// Break up the message into smaller chunks
	splitMessage := splitData(message)

	// Send the file transfer initialization packet
	c.SendPackage(c.InitFileTransferPacket())

	// Send the actual data message string
	for i := 0; i < len(splitMessage); i++ {
		// Send a packet
		c.SendPackage(c.FileTransferPacket(splitMessage[i], uint16(i)))
		// Wait before sending next packet
		time.Sleep(20 * time.Microsecond)
	}

	time.Sleep(200 * time.Microsecond)

	// Send the file transfer close packet
	c.SendPackage(c.EndFileTransferPacket())

	return nil
}

func (c *SocketCall) saprusPayloadLength(pkt []byte) [2]byte {
	payloadLength := make([]byte, 2)
	var length [2]byte
	binary.BigEndian.PutUint16(payloadLength, uint16(len(pkt)))

	length[0] = payloadLength[0]
	length[1] = payloadLength[1]

	return length
}

func (c *SocketCall) SendPacket(buf gopacket.SerializeBuffer) error {
	_, err := c.conn.WriteTo(buf.Bytes(), &packet.Addr{HardwareAddr: layers.EthernetBroadcast})
	return err
}
