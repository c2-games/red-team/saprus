package utils

import (
	"log"
	"net"
)

func FindOriginInterface(remoteAddr []string) *net.Interface {
	var origin_interface *net.Interface = nil

	interfaces, _ := net.Interfaces()
	for _, ifc := range interfaces {
		ifc_ips, _ := ifc.Addrs()
		for _, ifc_ip := range ifc_ips {
			_, tmpNet, err := net.ParseCIDR(ifc_ip.String())
			if err != nil {
				log.Printf("ERROR: parsing CIDR %s\n", err)
				return nil
			}
			if tmpNet.Contains(net.ParseIP(remoteAddr[0])) {
				origin_interface, err = net.InterfaceByName(ifc.Name)
				if err != nil {
					log.Printf("ERROR: setting interface %s\n", err)
					return nil
				}
			}
		}
	}
	return origin_interface
}
