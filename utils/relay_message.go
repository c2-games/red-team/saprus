package utils

import (
	"encoding/binary"
	"log"
	"net"

	"saprus/structs"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/mdlayher/packet"
)

func getRelayAdapter() string {
	inetInterface := ""
	inetInterfaces, _ := net.Interfaces()
	if inetInterfaces[0].Name == "lo" {
		inetInterface = inetInterfaces[1].Name
	} else {
		inetInterface = inetInterfaces[0].Name
	}
	return inetInterface
}

func (c *SocketCall) RelayMessagePacket(message string, dest string) gopacket.SerializeBuffer {
	a := net.IP.To4(net.ParseIP(dest))
	dest_le := make([]byte, 5) // Leaving room for null termination
	binary.BigEndian.PutUint32(dest_le, binary.BigEndian.Uint32(a))
	sapPayload := structs.SaprusRelayMessage{
		Dest:    [4]byte{dest_le[0], dest_le[1], dest_le[2], dest_le[3]},
		Payload: []byte(message),
	}
	sapHeader := structs.SaprusHeaderFrame{
		PacketType: [2]byte{0x00, 0x3C},
		Length:     c.saprusPayloadLength(sapPayload.Bytes()),
		Payload:    sapPayload.Bytes(),
	}

	buf := c.BuildRelayPacket(sapHeader, sapPayload)

	return buf
}

func (c *SocketCall) SendRelayMessage(message string, dest string) {
	iface, _ := net.InterfaceByName(getRelayAdapter())
	c.Iface = iface

	conn, err := packet.Listen(iface, packet.Raw, int(layers.EthernetTypeIPv4), nil)
	c.conn = conn

	if err != nil {
		log.Panic(err)
	}

	if len(message) > 1300 {
		log.Panic("message too large")
	}

	log.Printf("Sending Message\n")
	c.SendPacket(c.RelayMessagePacket(message, dest))
}

func (c *SocketCall) BuildRelayPacket(Header structs.SaprusHeaderFrame,
	Data structs.SaprusRelayMessage) gopacket.SerializeBuffer {
	eth, ip, udp := basePacket(c)

	sapHeader := structs.SaprusHeaderFrame{
		PacketType: Header.PacketType,
		Length:     c.saprusPayloadLength(Data.Bytes()),
		Payload:    Data.Bytes(),
	}

	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{
		ComputeChecksums: true,
		FixLengths:       true,
	}

	udp.SetNetworkLayerForChecksum(&ip)
	err := gopacket.SerializeLayers(buf, opts, &eth, &ip, &udp, gopacket.Payload(sapHeader.Bytes()))

	if err != nil {
		log.Panic(err)
	}

	return buf
}
