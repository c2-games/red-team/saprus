package utils

import (
	"github.com/google/gopacket/layers"
)

func basePacket(c *SocketCall) (layers.Ethernet, layers.IPv4, layers.UDP) {
	eth := layers.Ethernet{
		EthernetType: layers.EthernetTypeIPv4,
		SrcMAC:       c.Iface.HardwareAddr,
		DstMAC:       layers.EthernetBroadcast,
	}
	ip := layers.IPv4{
		Version:  4,
		TTL:      64,
		SrcIP:    []byte{0, 0, 0, 0},
		DstIP:    []byte{255, 255, 255, 255},
		Protocol: layers.IPProtocolUDP,
	}
	udp := layers.UDP{
		SrcPort: 6868,
		DstPort: 8888,
	}

	return eth, ip, udp
}
