package structs

type SaprusRelayMessage struct {
	Dest    [4]byte
	Payload []byte
}

func (c SaprusRelayMessage) Bytes() []byte {
	data := []byte{}
	data = append(data, c.Dest[0], c.Dest[1], c.Dest[2], c.Dest[3])
	data = append(data, c.Payload...)
	return data
}

func ParseRelayMessage(buf []byte) SaprusRelayMessage {
	message := SaprusRelayMessage{
		Dest:    [4]byte{byte(buf[0]), buf[1], buf[2], buf[3]},
		Payload: make([]byte, len(buf[4:])),
	}
	copy(message.Payload, buf[4:])
	return message
}
