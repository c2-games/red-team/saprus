package structs

type SaprusFileTransfer struct {
	Flag     [1]byte
	Sequence [2]byte
	Checksum [2]byte
	Payload  []byte
}

func (c SaprusFileTransfer) Bytes() []byte {
	data := []byte{}
	data = append(data, c.Flag[0])
	data = append(data, c.Sequence[0], c.Sequence[1])
	data = append(data, c.Checksum[0], c.Checksum[1])
	data = append(data, c.Payload...)
	return data
}
