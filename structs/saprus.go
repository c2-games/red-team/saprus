package structs

type SaprusHeaderFrame struct {
	PacketType [2]byte
	Length     [2]byte
	Payload    []byte
}

func (c SaprusHeaderFrame) Bytes() []byte {
	data := []byte{}
	data = append(data, c.PacketType[0], c.PacketType[1])
	data = append(data, c.Length[0], c.Length[1])
	data = append(data, c.Payload...)
	return data
}
