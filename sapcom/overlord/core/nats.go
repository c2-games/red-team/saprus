// This file contains functions, etc. related to NATS

package core

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/nats-io/nats.go"

	nh "saprus/nats_helper"
)

// The active NATS connection
var natsConn *nats.Conn

// The active NATS JetStream Context
var natsJs nats.JetStreamContext

func createStream(streamName string, subjects []string) (*nats.StreamInfo, error) {
	// Define the stream configuration
	streamConfig := &nats.StreamConfig{
		Name:      streamName,        // Stream name
		Subjects:  subjects,          // List of subjects associated with this stream
		Storage:   nats.FileStorage,  // Store data in file system (persistent)
		Retention: nats.LimitsPolicy, // Retain messages based on limits
		MaxMsgs:   -1,                // Unlimited number of messages
		MaxAge:    0,                 // Messages never expire
	}

	// Create the stream
	stream, err := natsJs.AddStream(streamConfig)
	return stream, err
}

func natsSetup(c NatsConnectionDetails) error {
	var err error

	natsConn, err = nh.NatsConnectRetrier(
		fmt.Sprintf("nats://%s:%d", c.Url, c.Port),
		c.User,
		c.Pass,
		time.Second*5,
		6,
	)
	if err != nil {
		return fmt.Errorf("failed to connect to NATS: %w", err)
	}

	// Initialize JetStream
	natsJs, err = natsConn.JetStream()
	if err != nil {
		return fmt.Errorf("failed to initialize JetStream: %w", err)
	}

	// Sub for all registration requests
	if _, err = nh.NewNatsSub(natsConn, "registration.requests", registrationRequestsHandler); err != nil {
		return fmt.Errorf("failed subscribing to 'registration.requests' subject: %w", err)
	}
	debug.Info("NATS: Subscribing to subject: registration.requests")

	// Sub to all existing Sentinel relayRequests
	pattern := "sentinel.*.relayMessages"
	debug.Infof("Subscribing to NATS pattern: %s", pattern)
	if _, err = nh.NewNatsSub(natsConn, pattern, sentinelRelayMessagesHandler); err != nil {
		return fmt.Errorf("failed subscribing to '%s' subject: %w", pattern, err)
	}
	debug.Infof("Successfully subscribed to: %s", pattern)

	// Sub to "overlord.requests"
	if _, err = nh.NewNatsSub(natsConn, "overlord.requests", overlordRequestsHandler); err != nil {
		return fmt.Errorf("failed subscribing to 'overlord.requests' subject: %w", err)
	}
	debug.Info("NATS: Subscribing to subject: overlord.requests")

	// Create stream for all Sentinel messages
	if _, err = createStream("SENTINELS", []string{"sentinel.>"}); err != nil {
		return fmt.Errorf("failed creating stream 'SENTINELS': %w", err)
	}
	debug.Info("JetStream for 'SENTINELS' created or updated.")

	return nil
}

func registrationRequestsHandler(msg *nats.Msg) {
	// Decoded request message
	var req registrationRequest

	debug.Info("Registration request received!")

	// Decode JSON into req
	err := json.Unmarshal(msg.Data, &req)
	if err != nil {
		debug.Errorf("failed to parse registration request: %s", err)
		nh.ErrRespHelper("failed to parse registration request", err, msg)
		return
	}

	token, err := registerSentinel(context.Background(), req)
	if err != nil {
		debug.Errorf("failed to register Sentinel: %s", err)
		nh.ErrRespHelper("failed to register Sentinel", err, msg)
		return
	}

	// The subject for the Sentinel
	subject := "sentinel." + sanitizeString(req.Name)
	// The subject that the Sentinel will send messages on
	relayMessagesSubject := subject + ".relayMessages"

	// Subscribe to relay messages from the Sentinel
	if _, err = nh.NewNatsSub(natsConn, relayMessagesSubject, sentinelRelayMessagesHandler); err != nil {
		debug.Errorf("failed subscribing to '%s' subject: %s", relayMessagesSubject, err)
		nh.ErrRespHelper(fmt.Sprintf("failed subscribing to '%s' subject", relayMessagesSubject), err, msg)
		return
	}
	debug.Infof("Subscribing to subject: %s", relayMessagesSubject)

	// Send successful response
	resp := map[string]string{
		"token":   token,
		"subject": subject,
	}
	nh.SendResponse(resp, msg)
}

// Handle relay messages sent from Sentinels to NATS
func sentinelRelayMessagesHandler(msg *nats.Msg) {
	debug.Infof("Relay message received on subject: %s", msg.Subject)

	// The decoded relay message
	var msgJSON relayMessage

	// Decode JSON into msgJSON
	if err := json.Unmarshal(msg.Data, &msgJSON); err != nil {
		nh.ErrRespHelper("failed to decode message", err, msg)
		return
	}

	debug.Infof("Received relay message from sentinel: %s", msgJSON.Sentinel)

	// Create a new document for storage
	doc := relayMessage{
		docBase: docBase{
			Type: "relayMsg",
		},
		Sentinel:  msgJSON.Sentinel,
		Message:   msgJSON.Message,
		Timestamp: msgJSON.Timestamp,
	}

	if doc.Timestamp == "" {
		now := time.Now()
		doc.Timestamp = now.Format("2006-01-02 15:04:05 MST")
	}

	parsedTime, err := time.Parse("2006-01-02 15:04:05 MST", doc.Timestamp)
	if err != nil {
		debug.Errorf("failed to parse timestamp: %s", err)
		nh.ErrRespHelper("failed to parse timestamp", err, msg)
		return
	}
	epochString := fmt.Sprintf("%d", parsedTime.Unix())

	// sha256 hash of the message
	msgHash := hashString(doc.Message.(string))

	// Check if the Message is valid JSON
	data, isValidJSON := parseJSONMap(doc.Message.(string))
	if isValidJSON {
		doc.Message = data
	}

	// Generate a unique doc ID
	doc.DocID = fmt.Sprintf("relayMessage:%s:%s:%s", doc.Sentinel, epochString, msgHash)

	// Add the relay message to the database using CreateDoc
	docID, rev, err := db.CreateDoc(context.Background(), doc)
	if err != nil {
		debug.Errorf("failed writing relay message to database: %s", err)
		debug.Infof("document being stored: %+v", doc)
		nh.ErrRespHelper("failed writing relay message to database", err, msg)
		return
	}
	doc.DocID = docID
	doc.Rev = rev

	debug.Infof("Sentinel relay message stored: %s", doc.DocID)

	newRelayMessageDiscordMessage(doc.Message, doc.Sentinel)
	// Send successful response
	resp := map[string]string{
		"response": "ok",
	}
	nh.SendResponse(resp, msg)
}

// Handle requests sent to "overlord.requests" from the REST API
func overlordRequestsHandler(msg *nats.Msg) {
	var req nh.OverlordRequest

	if err := json.Unmarshal(msg.Data, &req); err != nil {
		debug.Errorf("failed to decode message: %s", err)
		nh.ErrRespHelper("failed to decode message", err, msg)
		return
	}

	switch req.Type {
	// If the request if for the PSK, return the PSK
	case nh.OReqPSK:
		resp := map[string]string{
			"psk": settings.PSK,
		}
		nh.SendResponse(resp, msg)

	// If the request is to validate a token, validate it and return whether it's valid or not.
	case nh.OReqTokenV:
		var sen sentinel
		var tData nh.TokenData = req.Data.(nh.TokenData)
		var err error
		var hashedToken string

		if err = getSentinelByName(context.Background(), tData.Sentinel, &sen); err != nil {
			debug.Errorf("failed getting sentinel data: %s", err)
			nh.ErrRespHelper("failed getting sentinel data", err, msg)
			return
		}

		if hashedToken, err = hashToken(tData.Token, sen.TokenSalt); err != nil {
			debug.Errorf("failed hashing token: %s", err)
			nh.ErrRespHelper("failed hashing token", err, msg)
			return
		}

		valid := validateToken(tData.Token, sen.TokenSalt, hashedToken)
		debug.Debugf("Token validation result: %t", valid)
		var resp = map[string]bool{
			"Valid": valid,
		}
		nh.SendResponse(resp, msg)
	}
}
