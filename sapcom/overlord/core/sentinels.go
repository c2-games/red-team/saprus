// This file contains the code related to managing connected sentinels

package core

import (
	"context"
	"fmt"
	"log"

	"github.com/go-kivik/kivik/v4"
)

// Function to register a sentinel; returns registration token (string) and error
func registerSentinel(c context.Context, req registrationRequest) (string, error) {
	if req.Psk != settings.PSK {
		return "", fmt.Errorf("unauthorized; bad pre-shared key")
	}

	if unique, err := checkUniqueSentinel(c, req.Name); err != nil {
		return "", fmt.Errorf("failed looking up sentinel by name: %w", err)
	} else if !unique {
		return "", fmt.Errorf("sentinel with provided name already exists. Use issued token")
	}

	// Issue a token and store it
	var tSentinel map[string]interface{} = make(map[string]interface{})
	token := generateToken() // Implement a token generator

	// Generate a secure salt
	salt, err := generateSalt()
	if err != nil {
		return "", fmt.Errorf("internal server error: failed to generate salt")
	}

	// Hash the token with the salt
	hash, err := hashToken(token, salt)
	if err != nil {
		return "", fmt.Errorf("internal server error: failed to hash token")
	}

	// Store the salt and hashed token in the sentinel
	tSentinel["_id"] = "sentinel:" + req.Name
	tSentinel["tokenSalt"] = salt
	tSentinel["tokenHash"] = hash
	// tSentinel["ip"] = req.IP
	tSentinel["type"] = "sentinel"
	tSentinel["name"] = req.Name
	// <SECTION END>

	// Store `tSentinel` in the database and return the token to the user
	if tSentinel["DocID"], tSentinel["Rev"], err = db.CreateDoc(c, tSentinel); err != nil {
		return "", fmt.Errorf("failed to register Sentinel in database: %w", err)
	}

	// Send a message to discord
	newSentinelDiscordMessage(req.Name)

	return token, nil
}

// Send a new sentinel message to the Discord web hook
func newSentinelDiscordMessage(name string) {
	title := "New Sentinel: " + name
	message := "New Sentinel added: " + name
	color := 32768
	if err := buildAndSendDiscordPayload(title, message, color); err != nil {
		log.Printf("ERROR: newSentinelDiscordMessage: failed sending Discord webhook message: %s", err)
	}
}

// Check if the sentinel name is unique
func checkUniqueSentinel(ctx context.Context, sentinelName string) (bool, error) {
	// Perform the query using the view "by_name" in the design document "_design/sentinelByName"
	rows := db.Query(ctx, "sentinelByName", "by_name", kivik.Param("key", sentinelName))
	defer rows.Close()

	// Check if any row exists for the sentinel name
	if rows.Next() {
		return false, nil // Name already exists
	}

	// Handle any error that occurred during iteration
	if err := rows.Err(); err != nil {
		return false, fmt.Errorf("error iterating over rows: %w", err)
	}

	return true, nil // Name is unique
}

// Helper function to get sentinel by name
func getSentinelByName(ctx context.Context, name string, sentinel *sentinel) error {
	// Query CouchDB by the "name" field in the "sentinel" documents
	row := db.Get(ctx, "sentinel:"+name) // Assuming document ID is structured like "sentinel:<name>"
	if err := row.ScanDoc(sentinel); err != nil {
		return fmt.Errorf("failed to retrieve sentinel: %w", err)
	}
	return nil
}
