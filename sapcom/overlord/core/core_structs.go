package core

import "context"

type StartupSettings struct {
	// Pre-Shared Key to use on startup
	PreSharedKey string `yaml:"psk"`
	// CouchDB connection settings
	DB DBConnectionSettings `yaml:"db"`
	// NATS connection settings
	NATS NatsConnectionDetails `yaml:"nats"`
	// Context
	Ctx context.Context
	// Path to a line delimited list of words to use for generating pre-shared keys
	WordsFile string `yaml:"words-file"`
	// URL for Discord webhook
	DiscordWebhook string `yaml:"discord-webhook"`
	// Debug level (0-3)
	DebugLevel int `yaml:"debug"`
}

type DBConnectionSettings struct {
	// URL for the database
	Url string `yaml:"url"`
	// User for the database
	User string `yaml:"user"`
	// Password for the database
	Pass string `yaml:"pass"`
	// Name of the database
	Name string `yaml:"name"`
	// Port used to connect to the database
	Port int `yaml:"port"`
	// SSL config for connecting to the database
	SslMode string `yaml:"ssl-mode"`
	// Database timezone
	Tz string `yaml:"tz"`
}

type registrationRequest struct {
	Psk  string `json:"psk" binding:"required"`
	Name string `json:"name" binding:"required"`
}
