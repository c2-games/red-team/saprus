// This file contains code related to the Discord Webhook

package core

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func buildAndSendDiscordPayload(title string, message interface{}, color int) error {
	if len(settings.DiscordWebhook) > 0 {
		var messageStr string

		switch v := message.(type) {
		// Convert the map to a JSON string
		case map[string]interface{}:
			jsonData, err := json.MarshalIndent(message, "", "  ")
			if err != nil {
				return fmt.Errorf("buildAndSendDiscordPayload: failed to convert map to JSON: %w", err)
			}
			messageStr = "```json\n" + string(jsonData) + "\n```"
		// If message is already a string, set it directly
		case string:
			messageStr = v
		default:
			return fmt.Errorf("buildAndSendDiscordPayload: unsupported message type %T", message)
		}

		var payload = map[string]interface{}{
			"embeds": []map[string]interface{}{
				{
					"title":       title,
					"description": messageStr,
					"color":       color, // Blue color
				},
			},
		}

		if err := sendDiscordWebhookMessage(payload); err != nil {
			return fmt.Errorf("failed sending Discord webhook message: %w", err)
		}
	}
	return nil
}

// Send a message to the webhook, if specified.
func sendDiscordWebhookMessage(payload map[string]interface{}) error {
	jsonData, err := json.Marshal(payload)
	if err != nil {
		return fmt.Errorf("error encoding JSON: %v", err)
	}

	// Make the POST request
	resp, err := http.Post(settings.DiscordWebhook, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return fmt.Errorf("error sending request: %v", err)
	}
	defer resp.Body.Close()

	// Check for a successful response
	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	return nil
}

// checkDiscordWebhookURL validates if a provided URL is a valid Discord webhook
func checkDiscordWebhookURL(webhookURL string) (bool, error) {
	resp, err := http.Get(webhookURL)
	if err != nil {
		return false, fmt.Errorf("error making request: %v", err)
	}
	defer resp.Body.Close()

	// A valid Discord webhook should return 200 OK
	if resp.StatusCode == http.StatusOK {
		return true, nil
	}

	return false, fmt.Errorf("received unexpected status code: %d", resp.StatusCode)
}

// Send the relay message to the Discord web hook
func newRelayMessageDiscordMessage(message interface{}, sentinel string) {
	title := "Relay Message from Sentinel: " + sentinel
	color := 255

	if err := buildAndSendDiscordPayload(title, message, color); err != nil {
		log.Printf("ERROR: newRelayMessageDiscordMessage: failed sending Discord webhook message: %s", err)
	}
}
