package core

type docBase struct {
	// Document ID
	DocID string `json:"_id,omitempty"`
	// Document revision - omit if empty
	Rev string `json:"_rev,omitempty"`
	// Type of doc
	Type string `json:"type"`
}

type sapcomSettings struct {
	// Type should always be "settings"
	docBase
	// Pre-Shared Key to use
	PSK string `json:"psk"`
	// URL for Discord webhook
	DiscordWebhook string `json:"discord-webhook"`
	// List of words to use for random PSK generation
	WordsList []string `json:"words-list"`
}

type sentinel struct {
	// Type should always be "sentinel"
	docBase
	// Salt for the token
	TokenSalt string `json:"tokenSalt"`
	// Authentication token hash
	TokenHash string `json:"tokenHash"`
	// IP address
	IP string `json:"ip"`
	// List of endpoint IDs the sentinel is aware of
	Endpoints []string `json:"endpoints"`
	// Name - Must be unique among sentinels
	Name string `json:"name"`
}

type endpoint struct {
	// Type should always be "endpoint"
	docBase
	// IP address
	IP string `json:"ip"`
	// Mac address
	Mac string `json:"mac"`
	// List of clients
	SaprusClients []string `json:"saprusClients"`
	// Generic data interface
	Data interface{} `json:"data"`
}

type saprusClient struct {
	// Type should always be "saprusClient"
	docBase
	// Kind of SaprusClient (saprusClientKind.DocID)
	Kind string `json:"kind"`
}

type saprusClientKind struct {
	// Type should always be "saprusClientKind"
	docBase
}

type relayMessage struct {
	// This embeds DocID, Rev, and Type
	// Type of doc - should always be "relayMsg"
	docBase
	// The Sentinel that originally received the message
	Sentinel string `json:"sentinel" binding:"required"`
	// The message string
	Message interface{} `json:"message" binding:"required"`
	// Timestamp in YYYY-mm-DD HH:MM:SS format
	Timestamp string `json:"timestamp"`
	// Originating Saprus Client
	// SaprusClient string `json:"saprusClient"`
}
