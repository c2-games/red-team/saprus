// This file contains structs related to NATS

package core

type NatsConnectionDetails struct {
	Url  string `yaml:"url" json:"url"`
	Port int    `yaml:"port" json:"port"`
	User string `yaml:"user" json:"user"`
	Pass string `yaml:"pass" json:"pass"`
}
