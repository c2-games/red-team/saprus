// This file contains the functions, etc. related to generating tokens.

package core

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

// Generate a random token for authentication after using PSK
func generateToken() string {
	bytes := make([]byte, 16)
	rand.Read(bytes)
	return hex.EncodeToString(bytes)
}

// Generate a random salt
func generateSalt() (string, error) {
	salt := make([]byte, 16)
	_, err := rand.Read(salt)
	if err != nil {
		return "", fmt.Errorf("failed to generate salt: %w", err)
	}
	return base64.StdEncoding.EncodeToString(salt), nil
}

// Hash the token using bcrypt
func hashToken(token string, salt string) (string, error) {
	saltedToken := token + salt
	hash, err := bcrypt.GenerateFromPassword([]byte(saltedToken), bcrypt.DefaultCost)
	if err != nil {
		return "", fmt.Errorf("failed to hash token: %w", err)
	}
	return string(hash), nil
}

// Validates that 'token' + 'salt' produces the same bcrypt hash.
func validateToken(token, salt, hashedToken string) bool {
	saltedToken := token + salt
	err := bcrypt.CompareHashAndPassword([]byte(hashedToken), []byte(saltedToken))
	return err == nil
}
