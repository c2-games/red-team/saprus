package core

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"reflect"

	"github.com/go-kivik/kivik/v4"

	// The CouchDB driver
	_ "github.com/go-kivik/kivik/v4/couchdb"
)

// Global: Active Couch database
var db *kivik.DB

// couchConnect establishes a connection to CouchDB and returns a client.
func couchConnect(c DBConnectionSettings, ctx context.Context) (*kivik.Client, error) {
	var proto string = "https"
	if c.SslMode == "disable" {
		proto = "http"
	}

	// Construct the DSN using net/url to handle special characters
	dsn := &url.URL{
		Scheme: proto,
		User:   url.UserPassword(c.User, c.Pass),
		Host:   fmt.Sprintf("%s:%d", c.Url, c.Port),
		Path:   "/", // Optional, specifies the root path
	}

	// Create a new Kivik client
	client, err := kivik.New("couch", dsn.String())
	if err != nil {
		return nil, fmt.Errorf("failed to create Kivik client: %w", err)
	}

	// Ping the database to verify the connection
	if _, err := client.Ping(ctx); err != nil {
		return nil, fmt.Errorf("failed to ping CouchDB: %w", err)
	}

	// Create the database
	if err = client.CreateDB(ctx, c.Name); err != nil {
		if kivik.HTTPStatus(err) == http.StatusPreconditionFailed {
			log.Printf("Database %s exists.\n", c.Name)
			return client, nil
		}
		return nil, fmt.Errorf("failed to create database %s: %w", c.Name, err)
	}

	return client, nil
}

// ensureUsersDB checks if the _users database exists, and creates it if not.
func ensureUsersDB(ctx context.Context, client *kivik.Client) error {
	// Check if the _users database exists
	usersDb := client.DB("_users")
	if err := usersDb.Err(); err != nil {
		// If the error is a 404, the database doesn't exist
		if kivik.HTTPStatus(err) == 404 {
			// Attempt to create the _users database
			if err := client.CreateDB(ctx, "_users"); err != nil {
				return fmt.Errorf("failed to create _users database: %w", err)
			}
			log.Println("_users database created successfully")
		} else {
			// If it's a different error, return it
			return fmt.Errorf("error checking _users database: %w", err)
		}
	} else {
		log.Println("_users database already exists")
	}

	return nil
}

func getDocById(ctx context.Context, docID string, doc interface{}) error {
	err := db.Get(ctx, docID).ScanDoc(doc)
	if err != nil {
		return err
	}

	return nil
}

func writeDoc(ctx context.Context, docID string, doc interface{}) error {
	newRev, err := db.Put(ctx, docID, doc)
	if err != nil {
		return err
	}

	// Use reflection to set the Rev field
	v := reflect.ValueOf(doc)
	if v.Kind() != reflect.Ptr || v.Elem().Kind() != reflect.Struct {
		return errors.New("doc must be a pointer to a struct")
	}

	// Get the underlying struct
	v = v.Elem()

	// Find the Rev field and set it
	revField := v.FieldByName("Rev")
	if !revField.IsValid() || !revField.CanSet() || revField.Kind() != reflect.String {
		return errors.New("doc does not have a settable Rev field of type string")
	}

	revField.SetString(newRev)

	return nil
}

func createDefaultDesignDocs(ctx context.Context) error {
	var designDocs []map[string]interface{}

	// Append the design document to the slice
	designDocs = append(designDocs, map[string]interface{}{
		"_id": "_design/sentinelByName",
		"views": map[string]interface{}{
			"by_name": map[string]interface{}{
				"map": `function(doc) { if (doc.type === 'sentinel') { emit(doc.name, null); } }`,
			},
		},
	})

	for i := range designDocs {
		// Ensure the _id is a string before passing it to db.Put
		id, ok := designDocs[i]["_id"].(string)
		if !ok {
			return fmt.Errorf("failed to assert _id as string")
		}

		// Get design doc if it already exists
		if err := getDocById(ctx, id, &designDocs[i]); err != nil {
			// Check if the error is a "not found" error (e.g., 404)
			if kivik.HTTPStatus(err) == 404 {
				// Document does not exist; we will create it from scratch, so we ignore this error
				log.Printf("Design document with ID %s does not exist, proceeding to create it.", id)
			} else {
				// For any other error, we handle it
				return fmt.Errorf("failed getting existing design doc: %w", err)
			}
		}

		// Create or update the design document in the database
		rev, err := db.Put(ctx, id, designDocs[i])
		if err != nil {
			return fmt.Errorf("failed to create or update design document: %w", err)
		}

		log.Printf("Design document created or updated, new revision: %s\n", rev)
	}

	return nil
}
