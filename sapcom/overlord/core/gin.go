package core

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func startAPI(apiPort int) {
	// Start the API server
	router := setupRouter()

	// Create an HTTP server with Gin's router
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", apiPort),
		Handler: router,
	}

	// Run server in a goroutine
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("ERROR: API server failed to start: %s", err)
		}
	}()

	log.Printf("API server is running on port %d", apiPort)
}

func setupRouter() *gin.Engine {
	r := gin.Default()

	// Health check endpoint
	r.GET("/healthz", func(c *gin.Context) {
		// Check NATS connection
		if natsConn == nil || !natsConn.IsConnected() {
			c.JSON(http.StatusServiceUnavailable, gin.H{
				"status": "unhealthy",
				"reason": "NATS connection not established",
				"time":   time.Now().Format(time.RFC3339),
			})
			return
		}

		// Check CouchDB connection
		if db == nil {
			c.JSON(http.StatusServiceUnavailable, gin.H{
				"status": "unhealthy",
				"reason": "Database connection not established",
				"time":   time.Now().Format(time.RFC3339),
			})
			return
		}

		// Check if we can ping CouchDB
		if _, err := db.Client().Ping(c.Request.Context()); err != nil {
			c.JSON(http.StatusServiceUnavailable, gin.H{
				"status": "unhealthy",
				"reason": "Database ping failed",
				"time":   time.Now().Format(time.RFC3339),
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status": "healthy",
			"time":   time.Now().Format(time.RFC3339),
		})
	})

	return r
}
