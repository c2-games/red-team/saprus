// This file contains code related to managing the application settings

package core

import (
	"bufio"
	"context"
	"crypto/rand"
	"fmt"
	"log"
	"math/big"
	"os"
	"strings"
)

func setupSettingsDoc(s StartupSettings) error {
	var err error

	// Attempt to get settings doc by name
	if err = getDocById(s.Ctx, "settings", &settings); err != nil {
		log.Printf("Unable pull settings doc by name; trying another way. Err: %s", err)

		// Attempt to retrieve the document
		if err = getSettingsDoc(s.Ctx); err != nil {
			// Check if the error indicates the document was not found
			// if kivik.HTTPStatus(err) == http.StatusNotFound {
			if err.Error() == "no document with type 'settings' found" {
				log.Println("Settings document not found. Creating a new one.")

				var initSettings = map[string]interface{}{
					"_id":             "settings",
					"type":            settings.Type,
					"psk":             settings.PSK,
					"discord-webhook": settings.DiscordWebhook,
					"words-list":      settings.WordsList,
				}

				settings.Type = "settings"
				if settings.DocID, settings.Rev, err = db.CreateDoc(s.Ctx, initSettings); err != nil {
					return fmt.Errorf("settings doc not found and failed to create: %w", err)
				}
			}
		}
	}

	// If words file is specified, add the words to the settings
	if len(s.WordsFile) > 3 {
		if err = readWordsFile(s.WordsFile); err != nil {
			log.Printf("ERROR: failed reading in words file: %s", err)
		}
	}

	if len(settings.WordsList) < 6 {
		log.Printf("WARNING: not enough words in words list. Generating random PSKs may fail.")
	}

	if settings.PSK, err = setupPreSharedKey(s.PreSharedKey, s.Ctx); err != nil {
		return fmt.Errorf("failed setting up pre-shared key: %w", err)
	}
	log.Println("PSK: " + settings.PSK)

	// Setup the Discord Webhook var if its set
	if len(s.DiscordWebhook) > 10 {
		var valid bool
		var tErr error
		if valid, tErr = checkDiscordWebhookURL(s.DiscordWebhook); tErr != nil {
			log.Printf("ERROR: Invalid Discord webhook. Error: %s", tErr)
		}

		if valid {
			settings.DiscordWebhook = s.DiscordWebhook
		}
	}

	// Save the updated settings doc
	if err = writeDoc(s.Ctx, settings.DocID, &settings); err != nil {
		return fmt.Errorf("failed updating settings doc: %w", err)
	}

	return nil
}

// getSettingsDoc retrieves the document of type "settings" from CouchDB
func getSettingsDoc(ctx context.Context) error {
	// Define the Mango query to find the document with type "settings"
	selector := map[string]interface{}{
		"selector": map[string]interface{}{
			"type": "settings",
		},
	}

	// Perform the query
	result := db.Find(ctx, selector)
	if result.Err() != nil {
		return fmt.Errorf("failed to query for settings document: %w", result.Err())
	}

	// Since there should only be one document of type "settings", retrieve the first result
	if !result.Next() {
		if err := result.Err(); err != nil {
			return fmt.Errorf("failed to iterate through results: %w", err)
		}
		return fmt.Errorf("no document with type 'settings' found")
	}

	// Extract the document into a map
	if err := result.ScanDoc(&settings); err != nil {
		return fmt.Errorf("failed to scan settings document: %w", err)
	}

	return nil
}

func setupPreSharedKey(psk string, ctx context.Context) (string, error) {
	var err error

	var l int = len(psk)

	// Update the stored PSK if the one provided is different and valid
	if l >= 6 && psk != settings.PSK {
		fmt.Println("Updated Pre-Shared Key.")
	} else if l < 6 && len(settings.PSK) < 6 {
		fmt.Println("No pre-shared key specified. Generating a new PSK.")
		psk, err = genPreSharedKey(settings.WordsList)
		if err != nil {
			return "", fmt.Errorf("failed to generate pre-shared key: %w", err)
		}
		settings.PSK = psk
		if err = writeDoc(ctx, settings.DocID, &settings); err != nil {
			return "", fmt.Errorf("failed updating settings doc: %w", err)
		}

		log.Printf("New Pre-Shared Key generated")
	} else {
		psk = settings.PSK
	}

	return psk, nil
}

func readWordsFile(wordsFile string) error {
	// Open the file
	file, err := os.Open(wordsFile)
	if err != nil {
		return fmt.Errorf("could not open words file: %w", err)
	}
	defer file.Close()

	// Read words into a slice
	var words []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		word := strings.TrimSpace(scanner.Text())
		if word != "" {
			words = append(words, word)
		}
	}
	if err := scanner.Err(); err != nil {
		return fmt.Errorf("could not read words file: %w", err)
	}

	// Ensure there are enough words
	if len(words) < 6 {
		return fmt.Errorf("not enough words in file to generate a key")
	}

	settings.WordsList = words
	return nil
}

// Generates a Pre-Shared Key from 6 random words from the provided slice
func genPreSharedKey(words []string) (string, error) {

	// Select 6 random words
	var selectedWords []string
	for i := 0; i < 6; i++ {
		n, err := rand.Int(rand.Reader, big.NewInt(int64(len(words))))
		if err != nil {
			return "", fmt.Errorf("could not generate random index: %w", err)
		}
		selectedWords = append(selectedWords, words[n.Int64()])
	}

	// Join the selected words with spaces to form the key
	return strings.Join(selectedWords, " "), nil
}
