// Helper functions

package core

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"regexp"
	"strings"
)

// Hashes a string into a sha256 hash string
func hashString(message string) string {
	// Create a new SHA-256 hash
	hasher := sha256.New()

	// Write the message to the hasher
	hasher.Write([]byte(message))

	// Get the hash as a byte slice and convert to a hex string
	hash := hasher.Sum(nil)
	return hex.EncodeToString(hash)
}

// Takes a map and uses json.MarshalIndent to produce a pretty-printable []byte for
// logs, stdout, or other human-readable needs
// func prettyMap(v any) (out []byte, err error) {
// 	out, err = json.MarshalIndent(v, "", "  ")
// 	return out, err
// }

// Takes a map, passes it to PrettyMap, then prints the result
// func prettyMapPrint(v any) (err error) {
// 	var out []byte
// 	out, err = prettyMap(v)
// 	if err != nil {
// 		return err
// 	}
// 	fmt.Println(string(out))
// 	return nil
// }

func parseJSONMap(data string) (interface{}, bool) {
	// Create a variable to hold the unmarshaled data
	var result interface{}

	// Attempt to unmarshal the string
	err := json.Unmarshal([]byte(data), &result)

	// If there's no error, it means the string is a valid JSON
	if err != nil {
		return nil, false
	}
	return result, true
}

// sanitizeString cleans up a string by:
// 1. Trimming leading and trailing whitespace.
// 2. Replacing spaces and special characters with underscores.
func sanitizeString(input string) string {
	// Trim leading and trailing whitespace
	s := strings.TrimSpace(input)

	// Replace spaces and special characters with underscores
	re := regexp.MustCompile(`[^\w]+`) // Matches any non-word character
	s = re.ReplaceAllString(s, "_")

	// Remove any leading or trailing underscores
	s = strings.Trim(s, "_")

	return s
}
