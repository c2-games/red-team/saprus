// This file contains global variables for the core package.

package core

import (
	lh "saprus/log_helper"
)

// The active system settings
var settings sapcomSettings

// The active debug logger
var debug *lh.Logger
