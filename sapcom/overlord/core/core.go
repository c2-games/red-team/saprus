package core

import (
	"fmt"
	"log"

	lh "saprus/log_helper"
)

func Startup(s StartupSettings) error {

	// Initialize the debug logger
	debug = lh.NewLoggerFromIntMust(s.DebugLevel)

	// Start the API server for health checks
	startAPI(8080)

	// Establish a connection to CouchDB
	client, err := couchConnect(s.DB, s.Ctx)
	if err != nil {
		return fmt.Errorf("error connecting to CouchDB: %w", err)
	}

	// Access the database
	db = client.DB(s.DB.Name)
	if err = db.Err(); err != nil {
		return fmt.Errorf("failed to open database '%s': %w", s.DB.Name, err)
	}

	// Create the _users database if it doesn't exist
	if err = ensureUsersDB(s.Ctx, client); err != nil {
		return fmt.Errorf("failed setting up _users database: %w", err)
	}

	// Setup the settings doc
	if err = setupSettingsDoc(s); err != nil {
		return fmt.Errorf("failed setting up settings doc '%s': %w", s.DB.Name, err)
	}

	// Create the default design docs
	if err = createDefaultDesignDocs(s.Ctx); err != nil {
		return fmt.Errorf("failed to create default design docs: %w", err)
	}

	// NATS startup
	if err = natsSetup(s.NATS); err != nil {
		return fmt.Errorf("failed NATS startup: %w", err)
	}

	// Listen for the context cancellation
	<-s.Ctx.Done()
	log.Println("Shutdown signal received, shutting down server...")

	// Gracefully close the NATS connection
	if natsConn != nil && !natsConn.IsClosed() { // Check if connection is valid and open
		natsConn.Drain() // Ensures all pending messages are sent before closing
		natsConn.Close()
		log.Println("NATS connection closed")
	} else {
		log.Println("NATS connection already closed or invalid")
	}

	log.Println("Server exited properly")
	return nil
}
