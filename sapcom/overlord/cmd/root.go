package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/c2-games/red-team/saprus/sapcom/overlord/core"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "sapcom-overlord",
	Short: "Primary control application for SapCom",
	Long: `Primary control application for SapCom
Note: CLI flags will override settings defined in config files or environmental variables`,
	Run: func(cmd *cobra.Command, args []string) {
		// Set up signal handling context
		ctx := setupSignalHandler()

		if err := startup(ctx); err != nil {
			fmt.Printf("Application error: %v\n", err)
			os.Exit(1)
		}

		fmt.Println("Application exited gracefully.")
	},
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVarP(&conn.Pass, "nats-pass", "a", conn.Pass,
		"Password for NATS")
	viper.BindPFlag("nats.pass", rootCmd.PersistentFlags().Lookup("nats-pass"))

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "",
		"Config file path (default is /etc/sapcom/config.yaml)")
	viper.BindPFlag("config", rootCmd.PersistentFlags().Lookup("config"))

	rootCmd.PersistentFlags().BoolVarP(&daemonMode, "daemon-mode", "d", daemonMode,
		"Run as daemon in background. Requires log-file (-l) to also be set.")
	viper.BindPFlag("daemon-mode", rootCmd.PersistentFlags().Lookup("daemon-mode"))

	rootCmd.PersistentFlags().StringVarP(&conn.User, "nats-user", "e", conn.User,
		"User with which to connect NATS")
	viper.BindPFlag("nats-user", rootCmd.PersistentFlags().Lookup("nats-user"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.PreSharedKey, "psk", "k", "",
		"Pre-shared authentication key")
	viper.BindPFlag("psk", rootCmd.PersistentFlags().Lookup("psk"))

	rootCmd.PersistentFlags().StringVarP(&logFilePath, "log-file", "l", logFilePath,
		"Path for the log file when running as a daemon")
	viper.BindPFlag("log-file", rootCmd.PersistentFlags().Lookup("log-file"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.DiscordWebhook, "discord-webhook", "m", "",
		"URL for Discord Webhook")
	viper.BindPFlag("discord-webhook", rootCmd.PersistentFlags().Lookup("discord-webhook"))

	rootCmd.PersistentFlags().StringVarP(&conn.Name, "db-name", "n", conn.Name,
		"Name of the database to connect to")
	viper.BindPFlag("db.name", rootCmd.PersistentFlags().Lookup("db-name"))

	rootCmd.PersistentFlags().StringVarP(&conn.Pass, "db-pass", "p", conn.Pass,
		"Password for the database")
	viper.BindPFlag("db.pass", rootCmd.PersistentFlags().Lookup("db-pass"))

	rootCmd.PersistentFlags().IntVarP(&conn.Port, "db-port", "P", conn.Port,
		"Port used to connect to the database")
	viper.BindPFlag("db.port", rootCmd.PersistentFlags().Lookup("db-port"))

	rootCmd.PersistentFlags().StringVarP(&conn.Url, "db-url", "r", conn.Url,
		"URL for the database")
	viper.BindPFlag("db.url", rootCmd.PersistentFlags().Lookup("db-url"))

	rootCmd.PersistentFlags().StringVarP(&conn.SslMode, "db-ssl-mode", "s", conn.SslMode,
		"SSL config for connecting to the database")
	viper.BindPFlag("db.ssl-mode", rootCmd.PersistentFlags().Lookup("db-ssl-mode"))

	rootCmd.PersistentFlags().IntVarP(&conn.Port, "nats-port", "t", conn.Port,
		"Port used to connect to NATS")
	viper.BindPFlag("nats.port", rootCmd.PersistentFlags().Lookup("nats-port"))

	rootCmd.PersistentFlags().StringVarP(&conn.User, "db-user", "u", conn.User,
		"User with which to connect to the database")
	viper.BindPFlag("db-user", rootCmd.PersistentFlags().Lookup("db-user"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.WordsFile, "words-file", "w", "/etc/sapcom/orchard-street-alpha.txt",
		"Path to a line delimited list of words to use for generating pre-shared keys")
	viper.BindPFlag("words-file", rootCmd.PersistentFlags().Lookup("words-file"))

	rootCmd.PersistentFlags().StringVarP(&conn.Url, "nats-url", "y", conn.Url,
		"URL for NATS")
	viper.BindPFlag("nats.url", rootCmd.PersistentFlags().Lookup("nats-url"))

	rootCmd.PersistentFlags().StringVarP(&conn.Tz, "db-tz", "z", conn.Tz,
		"Database timezone")
	viper.BindPFlag("db.tz", rootCmd.PersistentFlags().Lookup("db-tz"))

	rootCmd.PersistentFlags().IntVarP(&debugLevel, "debug", "v", debugLevel,
		"Debug level (0-4). Higher numbers = more verbose output")
	viper.BindPFlag("debug", rootCmd.PersistentFlags().Lookup("debug"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	// First load config file
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Search config in default directories directory with name "config" (without extension).
		viper.AddConfigPath("/etc/sapcom")
		viper.AddConfigPath("/opt/sapcom/config")
		viper.AddConfigPath("./config")
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Printf("Using config file: %s", viper.ConfigFileUsed())
		cfgFile = viper.ConfigFileUsed()
	}

	// Setup ENV vars in format of "OVERLORD_VAR". Ex: OVERLORD_DB_URL; OVERLORD_API_PORT
	// ENV vars will take precedence over settings defined in the config file
	viper.SetEnvPrefix("OVERLORD")
	// Replace hyphens with underscores in keys
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	// Read in environment variables that match
	viper.AutomaticEnv()

	// Set the vars from switches; takes precedence over ENV vars and Config file.
	cmd := rootCmd
	cmd.Flags().Visit(func(f *pflag.Flag) {
		// Only override if flag was explicitly set
		if f.Changed {
			viper.Set(f.Name, f.Value.String())
		}
	})

	// Now set the variables with the proper precedence applied
	setVars()
	// preStart()
}

func setVars() {
	// The values will come from viper with proper precedence:
	// 1. Command line flags (if set)
	// 2. Environment variables (if set)
	// 3. Config file values (if set)
	// 4. Default values (if any)

	conn.Name = viper.GetString("db.name")
	conn.User = viper.GetString("db.user")
	conn.Pass = viper.GetString("db.pass")
	conn.Url = viper.GetString("db.url")
	conn.Port = viper.GetInt("db.port")
	conn.SslMode = viper.GetString("db.ssl-mode")
	conn.Tz = viper.GetString("db.tz")
	// conn.ApiPort = viper.GetInt("api-port")
	nats.User = viper.GetString("nats.user")
	nats.Pass = viper.GetString("nats.pass")
	nats.Url = viper.GetString("nats.url")
	nats.Port = viper.GetInt("nats.port")
	startSettings.PreSharedKey = viper.GetString("psk")
	startSettings.WordsFile = viper.GetString("words-file")
	startSettings.DiscordWebhook = viper.GetString("discord-webhook")

	daemonMode = viper.GetBool("daemon-mode")
	logFilePath = viper.GetString("log-file")

	startSettings.DB = conn
	startSettings.NATS = nats

	// Set debug level
	debugLevel = viper.GetInt("debug")
	if debugLevel < 0 {
		debugLevel = 0
	} else if debugLevel > 4 {
		debugLevel = 4
	}
	startSettings.DebugLevel = debugLevel

	if len(conn.Pass) < 3 {
		fmt.Fprintln(os.Stderr, "ERROR: No/invalid database password specified. Exiting.")
		os.Exit(1)
	}

	if len(startSettings.PreSharedKey) < 6 && len(startSettings.WordsFile) < 5 {
		fmt.Fprintln(os.Stderr, "WARNING: No/invalid pre-shared key specified (len(psk) < 6) and "+
			"no/invalid words file provided (len(words-file) < 5). ")
	}
}

func setupSignalHandler() context.Context {
	ctx, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	go func() {
		sig := <-c
		fmt.Printf("Received signal %s. Shutting down...\n", sig)
		cancel()
	}()

	return ctx
}

func startup(ctx context.Context) error {

	startSettings.Ctx = ctx

	if daemonMode && os.Getenv("RUN_AS_DAEMON") != "true" {
		// Open (or create) a log file
		file, err := os.OpenFile(logFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatalf("Failed to open log file: %v", err)
		}

		log.Println("Restarting app in the background...")

		// Redirect stdout and stderr to the file
		os.Stdout = file
		os.Stderr = file
		log.SetOutput(file)

		// Re-run the app as a detached background process
		cmd := exec.Command(os.Args[0], os.Args[1:]...) // Pass along any command-line args
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		// Set an environment variable to prevent infinite recursion
		cmd.Env = append(os.Environ(), "RUN_AS_DAEMON=true")

		if err := cmd.Start(); err != nil {
			fmt.Printf("Failed to start in background: %v\n", err)
			os.Exit(1)
		}

		fmt.Printf("App is now running in the background with PID %d\n", cmd.Process.Pid)
		os.Exit(0) // Exit the parent process
	}

	err := core.Startup(startSettings)
	if err != nil {
		return fmt.Errorf("startup failed: %w", err)
	}

	return nil
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
