package cmd

import "gitlab.com/c2-games/red-team/saprus/sapcom/overlord/core"

// cfgFile stores the path to the configuration file
var cfgFile string

// The default database connection settings
var conn core.DBConnectionSettings = core.DBConnectionSettings{
	Url:     "localhost",
	User:    "sapcom",
	Pass:    "",
	Name:    "sapcom",
	Port:    5984,
	SslMode: "disable",
	Tz:      "US/Eastern",
}

// The default NATS connection settings
var nats core.NatsConnectionDetails = core.NatsConnectionDetails{
	Url:  "localhost",
	Port: 4222,
	User: "admin",
	Pass: "",
}

// Whether to run as a daemon in the background
var daemonMode bool

// Path to the log file when running as a daemon
var logFilePath string

var startSettings core.StartupSettings = core.StartupSettings{}

// Debug level for the application; default is 1 (error)
var debugLevel int = 1
