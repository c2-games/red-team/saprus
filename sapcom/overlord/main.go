package main

import (
	"saprus/logo"

	"gitlab.com/c2-games/red-team/saprus/sapcom/overlord/cmd"
)

func main() {
	logo.WelcomeMessage()

	cmd.Execute()
}
