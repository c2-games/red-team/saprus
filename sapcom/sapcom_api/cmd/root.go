package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/c2-games/red-team/saprus/sapcom/sapcom_api/core"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "sapcom_api",
	Short: "REST API interface for SapCom.",
	Long: `REST API interface for SapCom.
Note: CLI flags will override settings defined in config files or environmental variables`,
	Run: func(cmd *cobra.Command, args []string) {
		// Set up signal handling context
		ctx := setupSignalHandler()

		if err := startup(ctx); err != nil {
			fmt.Printf("Application error: %v\n", err)
			os.Exit(1)
		}

		fmt.Println("Application exited gracefully.")
	},
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().IntVarP(&startSettings.ApiPort, "api-port", "a", startSettings.ApiPort,
		"Port for the REST API")
	viper.BindPFlag("api-port", rootCmd.PersistentFlags().Lookup("api-port"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.SapComNatsURL, "nats-url", "b", startSettings.SapComNatsURL,
		"SapCom NATS server URL")
	viper.BindPFlag("nats-url", rootCmd.PersistentFlags().Lookup("nats-url"))

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "",
		"Config file path (default is /etc/sapcom/config.yaml)")
	viper.BindPFlag("config", rootCmd.PersistentFlags().Lookup("config"))

	rootCmd.PersistentFlags().BoolVarP(&daemonMode, "daemon-mode", "d", daemonMode,
		"Run as daemon in background. Requires log-file (-l) to also be set.")
	viper.BindPFlag("daemon-mode", rootCmd.PersistentFlags().Lookup("daemon-mode"))

	rootCmd.PersistentFlags().StringVarP(&logFilePath, "log-file", "l", logFilePath,
		"Path for the log file when running as a daemon")
	viper.BindPFlag("log-file", rootCmd.PersistentFlags().Lookup("log-file"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.SapComNatsPass, "nats-pass", "P", startSettings.SapComNatsPass,
		"SapCom NATS server password")
	viper.BindPFlag("nats-pass", rootCmd.PersistentFlags().Lookup("nats-pass"))

	rootCmd.PersistentFlags().IntVarP(&startSettings.SapComNatsPort, "nats-port", "s", startSettings.SapComNatsPort,
		"SapCom NATS server port")
	viper.BindPFlag("nats-port", rootCmd.PersistentFlags().Lookup("nats-port"))

	rootCmd.PersistentFlags().StringVarP(&startSettings.SapComNatsUser, "nats-user", "U", startSettings.SapComNatsUser,
		"SapCom NATS server username")
	viper.BindPFlag("nats-user", rootCmd.PersistentFlags().Lookup("nats-user"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Search config in default directories directory with name "config" (without extension).
		viper.AddConfigPath("/etc/sapcom-api")
		viper.AddConfigPath("/opt/sapcom-api/config")
		viper.AddConfigPath("./config")
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Printf("Using config file: %s", viper.ConfigFileUsed())
		cfgFile = viper.ConfigFileUsed()
	}

	// Setup ENV vars in format of "SAPCOM_VAR". Ex: SAPCOM_DB_URL; SAPCOM_API_PORT
	// ENV vars will take precedence over settings defined in the config file
	viper.SetEnvPrefix("SAPCOM_API")
	// Replace hyphens with underscores in keys
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	// Read in environment variables that match
	viper.AutomaticEnv()

	// Set the vars from switches; takes precedence over ENV vars and Config file.
	cmd := rootCmd
	cmd.Flags().Visit(func(f *pflag.Flag) {
		// Only override if flag was explicitly set
		if f.Changed {
			viper.Set(f.Name, f.Value.String())
		}
	})

	// Set the vars from switches; takes precedence over ENV vars and Config file.
	setVars()
}

func setVars() {
	startSettings.ApiPort = viper.GetInt("api-port")
	startSettings.SapComNatsURL = viper.GetString("nats-url")
	startSettings.SapComNatsUser = viper.GetString("nats-user")
	startSettings.SapComNatsPass = viper.GetString("nats-pass")
	startSettings.SapComNatsPort = viper.GetInt("nats-port")

	daemonMode = viper.GetBool("daemon-mode")
	logFilePath = viper.GetString("log-file")
}

func setupSignalHandler() context.Context {
	ctx, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	go func() {
		sig := <-c
		fmt.Printf("Received signal %s. Shutting down...\n", sig)
		cancel()
	}()

	return ctx
}

func startup(ctx context.Context) error {

	if len(startSettings.SapComNatsPass) < 4 || len(startSettings.SapComNatsURL) < 4 ||
		len(startSettings.SapComNatsUser) < 3 {
		log.Fatalln("Missing required startup parameters for NATS connection. Exiting.")
	}

	startSettings.Ctx = ctx

	if daemonMode && os.Getenv("RUN_AS_DAEMON") != "true" {
		// Open (or create) a log file
		file, err := os.OpenFile(logFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatalf("Failed to open log file: %v", err)
		}

		log.Println("Restarting app in the background...")

		// Redirect stdout and stderr to the file
		os.Stdout = file
		os.Stderr = file
		log.SetOutput(file)

		// Re-run the app as a detached background process
		cmd := exec.Command(os.Args[0], os.Args[1:]...) // Pass along any command-line args
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		// Set an environment variable to prevent infinite recursion
		cmd.Env = append(os.Environ(), "RUN_AS_DAEMON=true")

		if err := cmd.Start(); err != nil {
			fmt.Printf("Failed to start in background: %v\n", err)
			os.Exit(1)
		}

		fmt.Printf("App is now running in the background with PID %d\n", cmd.Process.Pid)
		os.Exit(0) // Exit the parent process
	}

	err := core.Startup(startSettings)
	if err != nil {
		return fmt.Errorf("startup failed: %w", err)
	}

	return nil
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		log.Fatalf("startup failed: %v", err)
	}
}
