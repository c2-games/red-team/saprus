package cmd

import "gitlab.com/c2-games/red-team/saprus/sapcom/sapcom_api/core"

// cfgFile stores the path to the configuration file
var cfgFile string

// Whether to run as a daemon in the background
var daemonMode bool

// Path to the log file when running as a daemon
var logFilePath string

var startSettings core.StartupSettings = core.StartupSettings{
	ApiPort:        14227,
	SapComNatsPort: 4222,
}
