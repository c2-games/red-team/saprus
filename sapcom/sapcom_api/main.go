package main

import (
	"saprus/logo"

	"gitlab.com/c2-games/red-team/saprus/sapcom/sapcom_api/cmd"
)

func main() {
	logo.WelcomeMessage()

	cmd.Execute()
}
