package core

import (
	"context"
	"fmt"
	"log"
	"time"

	nh "saprus/nats_helper"

	"github.com/nats-io/nats.go"
)

// var settings sapcomSettings

// The main NATS connection for SapCom
var natsConn *nats.Conn

// The URL for NATS
var natsURL string

func Startup(s StartupSettings) error {
	var err error

	natsURL = fmt.Sprintf("nats://%s:%d", s.SapComNatsURL, s.SapComNatsPort)

	// Connect to NATS with provided username & password
	if natsConn, err = nh.NatsConnectRetrier(
		natsURL, s.SapComNatsUser, s.SapComNatsPass, 3*time.Second, 100); err != nil {
		return fmt.Errorf("failed initial NATS connection: %v", err)
	}

	srv, err := apiStartup(s.ApiPort)
	if err != nil {
		return fmt.Errorf("failed api startup: %v", err)
	}

	// Listen for the context cancellation
	<-s.Ctx.Done()
	log.Println("Shutdown signal received, shutting down server...")

	// Create a context with timeout for the shutdown process
	ctxShutDown, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Attempt graceful shutdown
	if err := srv.Shutdown(ctxShutDown); err != nil {
		return fmt.Errorf("Server forced to shutdown: %v\n", err)
	}

	log.Println("Server exited properly")
	return nil
}
