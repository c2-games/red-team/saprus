package core

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	nh "saprus/nats_helper"

	"github.com/gin-gonic/gin"
	"github.com/nats-io/nats.go"
)

func apiStartup(apiPort int) (*http.Server, error) {
	var err error

	// Start the API server
	router := setupRouter()

	// Create an HTTP server with Gin's router
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", apiPort),
		Handler: router,
	}

	// Run server in a goroutine
	go func() {
		if err = srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("ERROR: API server failed to start: %s", err)
		}
	}()

	log.Printf("Server is running on port %d", apiPort)

	return srv, err
}

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	gin.SetMode("debug")
	r := gin.Default()

	// Health check endpoint for kubernetes/docker
	r.GET("/healthz", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status": "healthy",
			"time":   time.Now().Format(time.RFC3339),
		})
	})

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	// RegisterSentinel endpoint to validate PSK and issue a token
	r.POST("/registerSentinel", func(c *gin.Context) {
		registerSentinelHandler(c)
	})

	// Middleware to validate tokens
	r.Use(func(c *gin.Context) {
		validateTokenMiddleware(c)
	})

	// Protected Ping endpoint
	r.GET("/authPing", func(c *gin.Context) {
		// This will only be executed if the token is valid
		c.JSON(http.StatusOK, gin.H{"message": "pong"})
	})

	r.POST("/relayMessage", func(c *gin.Context) {
		relayMessageHandler(c)
	})

	return r
}

func registerSentinelHandler(c *gin.Context) {
	var err error
	var msgJSON nh.TokenReqResp

	var q struct {
		Psk  string `json:"psk" binding:"required"`
		Name string `json:"name" binding:"required"`
	}

	if err = c.BindJSON(&q); err != nil {
		c.JSON(
			http.StatusInternalServerError,
			gin.H{
				"error": err.Error(),
			},
		)
		return
	}

	if err = nh.RequestTokenAnon(natsURL, q.Psk, q.Name, &msgJSON); err != nil {
		c.JSON(
			http.StatusInternalServerError,
			gin.H{
				"error": err.Error(),
			},
		)
		return
	}

	c.JSON(http.StatusOK, gin.H{"token": msgJSON.Token, "subject": msgJSON.Subject})
}

// Middleware to validate tokens
func validateTokenMiddleware(c *gin.Context) {
	// Extract token from the Authorization header
	token := c.GetHeader("Authorization")
	if token == "" {
		c.AbortWithStatusJSON(
			http.StatusUnauthorized,
			gin.H{"status": "unauthorized", "message": "Missing token"},
		)
		return
	}

	// Extract the sentinel name from a header (or query parameter)
	name := c.GetHeader("Sentinel-Name")
	if name == "" {
		c.AbortWithStatusJSON(
			http.StatusUnauthorized,
			gin.H{"status": "unauthorized", "message": "Missing sentinel name"},
		)
		return
	}

	var req = nh.OverlordRequest{
		Type: nh.OReqTokenV,
		Data: nh.TokenData{
			Sentinel: name,
			Token:    token,
		},
	}

	var reqD []byte
	var err error
	if reqD, err = json.Marshal(req); err != nil {
		c.AbortWithStatusJSON(
			http.StatusInternalServerError,
			gin.H{"error": "failed marshalling JSON to send NATS request to Overlord"},
		)
		return
	}

	var msg *nats.Msg
	if msg, err = natsConn.Request("overlord.requests", reqD, 5*time.Second); err != nil {
		c.AbortWithStatusJSON(
			http.StatusInternalServerError,
			gin.H{"error": "failed sending NATS request to Overlord"},
		)
		return
	}

	var resp struct {
		Valid bool   `json:"valid"`
		Error string `json:"error"`
	}

	if err = json.Unmarshal(msg.Data, &resp); err != nil {
		c.AbortWithStatusJSON(
			http.StatusInternalServerError,
			gin.H{"error": "failed unmarshaling response data"},
		)
		return
	}

	if len(resp.Error) > 0 {
		c.AbortWithStatusJSON(
			http.StatusInternalServerError,
			gin.H{"error": fmt.Sprintf("overlord returned error: %s", resp.Error)},
		)
		return
	}

	if !resp.Valid {
		c.AbortWithStatusJSON(
			http.StatusUnauthorized,
			gin.H{"status": "unauthorized", "message": "Invalid sentinel & token combination"},
		)
		return
	}

	// Token is valid, proceed with the request
	c.Next()
}

func relayMessageHandler(c *gin.Context) {
	type relayMessagePost struct {
		Sentinel  string      `json:"sentinel" binding:"required"`
		Message   interface{} `json:"message" binding:"required"`
		Timestamp string      `json:"timestamp"`
	}

	var body = relayMessagePost{}
	var err error

	// Bind JSON payload to the struct
	if err = c.ShouldBindJSON(&body); err != nil {
		// If there's an error, respond with a 400 status and the error message
		c.JSON(
			http.StatusBadRequest,
			gin.H{"error": fmt.Sprintf("failed binding to JSON: %v", err)})
		return
	}

	if body.Timestamp == "" {
		now := time.Now()
		body.Timestamp = now.Format("2006-01-02 15:04:05 MST")
	}

	var bodyM []byte
	if bodyM, err = json.Marshal(body); err != nil {
		c.JSON(
			http.StatusInternalServerError,
			gin.H{
				"error": fmt.Sprintf("failed marshaling JSON: %v", err),
			},
		)
		return
	}

	var msg = nats.Msg{
		Subject: "sentinel." + body.Sentinel + ".relayMessages",
		Data:    bodyM,
	}

	if err = natsConn.PublishMsg(&msg); err != nil {
		c.JSON(
			http.StatusInternalServerError,
			gin.H{
				"error": fmt.Sprintf("failed publishing message: %v", err),
			},
		)
		return
	}

	// Respond with an OK message
	c.JSON(http.StatusOK, gin.H{"status": "received"})

}
