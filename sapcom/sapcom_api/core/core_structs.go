package core

import "context"

type StartupSettings struct {
	// Connection Settings
	ApiPort int `yaml:"api-port"`
	// Context
	Ctx context.Context
	// URL of the SapCom NATS server
	SapComNatsURL string `yaml:"sapcom-nats-url"`
	// User to connect to NATS with
	SapComNatsUser string `yaml:"sapcom-nats-user"`
	// Password to connect to NATS with
	SapComNatsPass string `yaml:"sapcom-nats-pass"`
	// Port of the SapCom NATS server
	SapComNatsPort int `yaml:"sapcom-nats-port"`
}
