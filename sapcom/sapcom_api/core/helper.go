// Helper functions

package core

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net"
)

// Hashes a string into a sha256 hash string
func hashString(message string) string {
	// Create a new SHA-256 hash
	hasher := sha256.New()

	// Write the message to the hasher
	hasher.Write([]byte(message))

	// Get the hash as a byte slice and convert to a hex string
	hash := hasher.Sum(nil)
	return hex.EncodeToString(hash)
}

// Takes a map and uses json.MarshalIndent to produce a pretty-printable []byte for
// logs, stdout, or other human-readable needs
func prettyMap(v any) (out []byte, err error) {
	out, err = json.MarshalIndent(v, "", "  ")
	return out, err
}

// Takes a map, passes it to PrettyMap, then prints the result
func prettyMapPrint(v any) (err error) {
	var out []byte
	out, err = prettyMap(v)
	if err != nil {
		return err
	}
	fmt.Println(string(out))
	return nil
}

func parseJSONMap(data string) (interface{}, bool) {
	// Create a variable to hold the unmarshaled data
	var result interface{}

	// Attempt to unmarshal the string
	err := json.Unmarshal([]byte(data), &result)

	// If there's no error, it means the string is a valid JSON
	if err != nil {
		return nil, false
	}
	return result, true
}

// validateIP checks if the given string is a valid IP address
func validateIP(ip string) bool {
	return net.ParseIP(ip) != nil
}
