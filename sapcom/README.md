# SapCom

## Deployment

### Using Docker Compose

1. Initialize environment configuration:

    ```bash
    make init-env
    ```

2. Review and customize the environment variables in `.env`:

    ```bash
    vim .env
    ```

    Key configurations to review:

    - Database credentials
    - NATS credentials
    - API ports
    - Service versions
    - Pre-shared key (PSK)

3. Change passwords in NATS config file:

    ```bash
    vim config/nats.conf
    ```

4. Build the images:

    ```bash
    # Build with cache
    make build-all

    # Build without cache
    make build-all-clean
    ```

5. Start the stack:

    ```bash
    make compose-up
    ```

6. Verify services are healthy:

    ```bash
    make compose-ps
    ```

### Service Dependencies

- **overlord**: Depends on healthy couch and nats services
- **api**: Depends on healthy overlord service
- **sentinel**: Depends on healthy api service
- **couch**: No dependencies
- **nats**: No dependencies

### Available Make Commands

#### Build Commands

- `make build-all` - Build all images using cache
- `make build-all-clean` - Build all images without cache
- `make build-api[-clean]` - Build API image [without cache]
- `make build-overlord[-clean]` - Build Overlord image [without cache]
- `make build-sentinel[-clean]` - Build Sentinel image [without cache]
- `make build-sapcom-all` - Build all SapCom images locally

#### Compose Commands

- `make compose-up` - Start all services
- `make compose-down` - Stop all services
- `make compose-down-volumes` - Stop services and remove volumes
- `make compose-logs` - View service logs
- `make compose-ps` - Check service status
- `make compose-validate` - Validate compose configuration
- `make compose-build` - Build service images
- `make compose-pull` - Pull latest service images
- `make compose-restart` - Restart all services

### Development Commands

- `make dev-init` - Initialize development environment (create network, volumes)
- `make dev-clean` - Clean up development environment
- `make dev-build` - Build all images locally
- `make dev-up` - Start all services in development mode
- `make dev-down` - Stop all services in development mode

## Architecture Overview

```mermaid
flowchart TD
  overlord("Overlord")
  nats("NATS")
  couch[("CouchDB")]
  api("SapCom REST API")
  sentinel("Sentinel")

  overlord <==> couch
  overlord <==> nats
  api <==> nats
  sentinel <==> api
  sentinel <==> nats
```

### Service Health Checks

All services implement health check endpoints:

- **overlord**: `GET /healthz` on port 8080
  - `curl http://localhost:8080/healthz`
- **api**: `GET /healthz` on port 14227
  - `curl http://localhost:14227/healthz`
- **couch**: `GET /_up` on port 5984
  - `curl http://localhost:5984/_up`
- **nats**: `GET /healthz` on port 8222
  - `curl http://localhost:8222/healthz`

Services will not start until their dependencies are healthy.

### Sentinel Registration Non-REST Workflow

```mermaid
sequenceDiagram
  participant Sentinel
  participant NATS
  participant Overlord
  participant CouchDB

  Sentinel->>NATS: Publish to registration.requests {name, psk}
  Overlord->>NATS: Subscribe to registration.requests
  NATS-->>Overlord: Deliver registration request
  Overlord ->> Overlord: Issue Sentinel token
  Overlord->>CouchDB: Store Sentinel Data
  CouchDB-->>Overlord: Success/Failure
  Overlord->>NATS: Publish response to reply subject (token + subject)
  NATS-->>Sentinel: Deliver response
```

### Sentinel Registration REST Workflow with NATS

```mermaid
sequenceDiagram
  participant Sentinel
  participant SapCom-API
  participant NATS
  participant Overlord
  participant CouchDB

  Sentinel->>SapCom-API: POST to /register {name, psk}
  SapCom-API->>NATS: Publish to registration.requests {name, psk}
  Overlord->>NATS: Subscribe to registration.requests
  NATS-->>Overlord: Deliver registration request
  Overlord ->> Overlord: Issue Sentinel token
  Overlord->>CouchDB: Store Sentinel Data
  CouchDB-->>Overlord: Success/Failure
  Overlord->>NATS: Publish response to reply subject (token + subject)
  NATS-->>SapCom-API: Deliver response
  SapCom-API-->>Sentinel: Respond to initial post
```
