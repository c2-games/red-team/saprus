package nats_helper

import (
	"encoding/json"
	"fmt"
)

// Struct for token request responses from Overlord.
type TokenReqResp struct {
	Error   string `json:"error,omitempty"`
	Token   string `json:"token"`
	Subject string `json:"subject"`
}

// OverlordRequestType is an "enum-like" type for requests
type OverlordRequestType string

const (
	OReqPSK    OverlordRequestType = "PSK"
	OReqTokenV OverlordRequestType = "ValidateToken"
)

func (ort OverlordRequestType) IsValid() bool {
	return ort == OReqPSK || ort == OReqTokenV
}

type TokenData struct {
	Sentinel string `json:"sentinel"`
	Token    string `json:"token"`
}

// Data struct for requesting data from Overlord via NATS
type OverlordRequest struct {
	// Type of request. Valid types: PSK, ValidateToken
	Type OverlordRequestType `json:"type"`

	// Additional data for the request. If Type == "Token", we'll store a TokenData struct.
	// If Type == "PSK", we skip storing anything here (Data == nil).
	Data interface{} `json:"data"`

	// Field to hold onto JSON temporarily during Unmarshal
	rawData json.RawMessage
}

// UnmarshalJSON performs custom JSON parsing to handle different request types.
func (or *OverlordRequest) UnmarshalJSON(b []byte) error {
	// Shadow struct matching OverlordRequest, but with rawData as json.RawMessage
	type alias OverlordRequest
	var tmp struct {
		alias
		Data json.RawMessage `json:"data"`
	}
	if err := json.Unmarshal(b, &tmp); err != nil {
		return err
	}
	*or = OverlordRequest(tmp.alias) // copy basic fields
	or.rawData = tmp.Data

	switch or.Type {
	case OReqPSK:
		// No additional data needed; set or.Data to nil
		or.Data = nil

	case OReqTokenV:
		var d TokenData
		if err := json.Unmarshal(or.rawData, &d); err != nil {
			return fmt.Errorf("failed to unmarshal TokenData: %w", err)
		}
		or.Data = d

	default:
		return fmt.Errorf("unknown request type: %s", or.Type)
	}

	return nil
}
