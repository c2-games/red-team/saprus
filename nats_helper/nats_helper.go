package nats_helper

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/nats-io/nats.go"
)

func NatsConnectRetrier(
	url string, user string, pass string, wait time.Duration, retries int) (
	conn *nats.Conn, err error) {

	for i := 0; i < retries; i++ {
		// Connect to NATS server
		conn, err = nats.Connect(url, nats.UserInfo(user, pass))
		if err != nil {
			// Wait before the next attempt
			time.Sleep(wait)
		} else {
			break
		}
	}

	return conn, nil
}

// Subscribe to a NATS subject and specify a handler function for processing messages from that subject
func NewNatsSub(
	natsConn *nats.Conn, subName string, handler func(msg *nats.Msg)) (
	sub *nats.Subscription, err error) {

	// Subscribe to the specified subject with the provided handler
	sub, err = natsConn.Subscribe(subName, handler)

	// Return any errors that occur during subscription
	return sub, err
}

// Function to send responses to received messages
func SendResponse(resp interface{}, msg *nats.Msg) {
	respData, _ := json.Marshal(resp)
	if msg.Reply != "" {
		msg.Respond(respData)
	}
}

// Function to help with handling errors in processing messages.
// Logs the error and responds to the original message.
func ErrRespHelper(errMsg string, err error, msg *nats.Msg) {
	log.Printf("%s: %v", errMsg, err)
	resp := map[string]string{
		"error": fmt.Sprintf("%s: %v", errMsg, err),
	}
	SendResponse(resp, msg)
}

// Function to register a Sentinel and get a token from Overlord as anonymous user.
func RequestTokenAnon(url string, psk string, sentinel string, msgJSON *TokenReqResp) error {
	// The temporary initial NATS connection
	var tNatsConn *nats.Conn
	var err error

	// Attempt to connect to NATS every 3 seconds until successful
	tNatsConn, err = NatsConnectRetrier(url, "anonymous", "", 3*time.Second, 100)
	if err != nil {
		return fmt.Errorf("failed to establish initial NATS connection: %v", err)
	}

	// The request message
	var dataMap = map[string]string{
		"psk":  psk,
		"name": sentinel,
	}

	var data []byte
	data, err = json.Marshal(dataMap)
	if err != nil {
		tNatsConn.Close()
		return fmt.Errorf("failed to marshal JSON for NATS initial connection: %v", err)
	}

	msg, err := tNatsConn.Request("registration.requests", data, 5*time.Second)
	if err != nil {
		tNatsConn.Close()
		return fmt.Errorf("failed to send registration request over NATS initial connection: %v", err)
	}

	err = json.Unmarshal(msg.Data, &msgJSON)
	if err != nil {
		tNatsConn.Close()
		return fmt.Errorf("failed to unmarshal NATS registration response message: %v", err)
	}

	if len(msgJSON.Error) > 0 {
		tNatsConn.Close()
		return fmt.Errorf("failed registration via NATS: %v", err)
	}

	return nil
}
